from src import storage
import logging as log

PATH = "data/switches/strong_dataset"
PATH2 = "data/switches/"

log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)

TEST = "strong_dataset"
NEW_TEST = "new_strong_dataset"

a = open(PATH, "r")

b = storage.load({}, PATH2, static_name=TEST, csv_file = True)

new = []
for elem in b:
    
    prefs = elem["prefix-list"]
    tims = elem["time-list"]
    
    new_prefs = []
    valid = False
    current_pref = ""
    for char in prefs:
        if char == "]":
            break
        elif char == "'":
            valid = not valid
            if valid == False:
                new_prefs.append(current_pref)
                current_pref = ""
        elif valid:
            current_pref += char

    new_tims = tims.replace(",","").replace('"','').replace("[","").replace("]","").split(" ")
    """
    current_tims = ""
    for char in tims:
        print(char)
        if char == "[":
            start = True
            print("a")
        elif char == "]":
            print("b")
            break
        elif (not char == " " or not char == ",") and start:
            print("c")
            current_tims += char
        elif char == ",":
            print("d")
            new_tims.append(str(float(current_tims)))
            current_tims = ""
    """
    
    elem["post-route"] = elem["post-route"].replace('"','') 
    elem["pre-route"] = elem["pre-route"].replace('"','') 
    elem["backup-route"] = elem["backup-route"].replace('"','') 
    elem["prefix-list"] = ' '.join(new_prefs)
    elem["time-list"] = ' '.join(new_tims)

    new.append(elem)

storage.store(new, {}, PATH2, static_name=NEW_TEST, csv_file = True)
