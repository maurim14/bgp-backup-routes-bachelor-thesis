import logging as log

import src.graphs.backup_cases as backup_cases
from src.graphs.uptimes import uptimes
from src.graphs.prefixes_switches import prefixes_switched
from src.graphs.prefixes_uptime import prefixes_uptime
from src.graphs.time_diff_switches import time_diff_switches
from src.graphs.origin_count_dirty import origin_count_dirty
from src.graphs.spa_prefixes import spa_prefixes
from src.graphs.prefix_amount_bundle_size import prefix_amount_bundle_size
from src.graphs.uptime_bundle_size import uptime_bundle_size
from src.graphs.amsix_failure_compare import amsix_failure_compare
from src.graphs.amsix_failure_prefix_times import amsix_failure_prefix_times
from src.graphs.overall_update_compare import overall_update_compare
from src.graphs.cloudflare_failure_compare import cloudflare_failure_compare
from src.graphs.cloudflare_failure_prefix_times import cloudflare_failure_prefix_times
from src.graphs.akamai_failure_compare import akamai_failure_compare
from src.graphs.akamai_failure_prefix_times import akamai_failure_prefix_times
from src.graphs.spa_announcement_compare import spa_announcement_compare
from src.graphs.specific_bundle_sizes_amount_prefixes import specific_bundle_sizes_amount_prefixes
from src.graphs.multiple_vs_once_measurement import multiple_vs_once_measurement
from src.graphs.specific_bundle_sizes_percent import specific_bundle_sizes_percent
from src.graphs.decix_prefix_times import decix_prefix_times
from src.graphs.specific_bundle_size_amount import specific_bundle_size_amount
from src.graphs.one_success_week import one_success_week
from src.graphs.overall_week_update import overall_week_update
from src.graphs.two_prefixes_uptime import two_prefixes_uptime
from src.graphs.three_uptime import three_uptime
from src.graphs.four_uplink_changes import four_uplink_changes 
from src.graphs.five_path_len import five_path_len 

TARGET = "3320"
PATH_SPA_CMP = "data/spa_compare/"

def graph_feeder():

    #backup_cases.backup_cases(TARGET)
    #uptimes(TARGET)
    #prefixes_switched(TARGET)
    #prefixes_uptime(TARGET)
    #time_diff_switches(TARGET)
    #origin_count_dirty(TARGET)
    #spa_prefixes()
#    prefix_amount_bundle_size()
    #uptime_bundle_size()
    
# outages
#    amsix_failure_compare()
#    amsix_failure_prefix_times()
    #cloudflare_failure_compare()
    cloudflare_failure_prefix_times()
#    one_success_week()
    #akamai_failure_compare()
    #akamai_failure_prefix_times()
    #decix_prefix_times()

# special graphs
#    overall_update_compare()
#    spa_announcement_compare()
#    overall_week_update()

# uptime
    #two_prefixes_uptime()
    #three_uptime()

# bundle-size stuff
#    specific_bundle_sizes_amount_prefixes()
#    specific_bundle_sizes_percent()
#    specific_bundle_size_amount()

# multiple vs single measurement
    #multiple_vs_once_measurement()

# path changes
    #four_uplink_changes()
    #five_path_len()


from src import storage
from src.stream_handler import StreamHandler
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import pylab as pl

def announcement_graph(start, end, collector, target_as, peer, bundle_size = "1.0"):

    handler = StreamHandler(target_as, start, end, collector, peer)
    handler.collect_data_updates()

    prefixes = {}
    for elem in handler.data:

        if elem.type.lower() == "a":
            prefix = elem._maybe_field("prefix")

            if prefix in prefixes:
                prefixes[prefix] += 1
            else:
                prefixes[prefix] = 1

    data1 = sorted(list(prefixes.values()))
    
    meta_data = {}
    meta_data["start"] = start
    meta_data["end"] = end
    meta_data["peer"] = peer
    meta_data["collector"] = collector
    meta_data["singlefile"] = str(False)
    meta_data["target_as"] = target_as
    meta_data["bundle size"] = bundle_size

    local_data = storage.load(meta_data, "data/switches/")
    if not local_data:
        log.error("No suitable backup file found")
        exit()

    prefixes = {}
    for switch in local_data["finding"]:
        if len(switch["prefix-list"]) == 1:
            pref = switch["prefix-list"][0]
            if pref in prefixes:
                prefixes[pref] += 1
            else:
                prefixes[pref] = 1
    
    data2 = sorted(list(prefixes.values()))
    
    full_data = storage.load({}, PATH_SPA_CMP, static_name = "spa_compare")
    if full_data:
        if bundle_size in full_data["spa"]:
            full_data["spa"][bundle_size] += data2
        else:   
            full_data["spa"][bundle_size] = data2
        full_data["rest"] += data1

    else:
        full_data = {}
        full_data["spa"] = {bundle_size: data2}
        full_data["rest"] = data1
    
    storage.store(full_data, {}, PATH_SPA_CMP, static_name = "spa_compare")

    """
    xlabel="Announcements"
    ylabel="Distribution"

    fit1 = stats.norm.pdf(full_data["rest"], np.mean(full_data["rest"]), np.std(full_data["rest"]))  #this is a fitting indeed
    pl.plot(full_data["rest"],fit1,'-o', label="all prefixes")

    fit2 = stats.norm.pdf(full_data["spa"], np.mean(full_data["spa"]), np.std(full_data["spa"]))  #this is a fitting indeed
    pl.plot(full_data["spa"],fit2,'-o', label="SPA prefixes")

    # graph generation
    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.legend(loc='right')

    plt.show()
    """

def data_grabber():
    announcement_graph(start = "2021-07-22 00:00:00",
                      end = "2021-07-23 00:00:00",
                      collector = "rrc01", 
                      target_as = "3320", 
                      peer = "1828", 
                      bundle_size = "1.0")

    announcement_graph(start = "2021-07-22 00:00:00",
                      end = "2021-07-23 00:00:00",
                      collector = "rrc01", 
                      target_as = "3320", 
                      peer = "1828", 
                      bundle_size = "0.5")

    announcement_graph(start = "2021-07-22 00:00:00",
                      end = "2021-07-23 00:00:00",
                      collector = "rrc01", 
                      target_as = "3320", 
                      peer = "1828", 
                      bundle_size = "0.3")
    announcement_graph(start = "2021-05-03 00:00:00",
                      end = "2021-05-04 00:00:00",
                      collector = "rrc01", 
                      target_as = "3320", 
                      peer = "57695", 
                      bundle_size = "1.0")
    
    announcement_graph(start = "2021-05-03 00:00:00",
                      end = "2021-05-04 00:00:00",
                      collector = "rrc01", 
                      target_as = "3320", 
                      peer = "35266", 
                      bundle_size = "0.5")

    announcement_graph(start = "2021-05-03 00:00:00",
                      end = "2021-05-04 00:00:00",
                      collector = "rrc01", 
                      target_as = "3320", 
                      peer = "36924", 
                      bundle_size = "0.3")

def main():
    graph_feeder()

if __name__=="__main__":
    main()
