import logging as log
from src.helper_functions import clean_as_path

def read_rib(stream):
    
    log.info("Processing RIB")
    
    ls = []
    origin_grouper = {}
    origin_mapper = {}
    time = None
    for elem in stream:
        if time == None:
            time = elem.time
        pfx = elem.fields["prefix"]
        path = clean_as_path(elem._maybe_field("as-path"))
        orig = path.split(" ")[-1]

        if pfx not in origin_mapper or not origin_mapper[pfx] == orig:
          origin_mapper[pfx] = orig

        if orig not in origin_grouper:
            origin_grouper[orig] = {"prefix": [pfx], "path": [path]}
        else:
            if pfx not in origin_grouper[orig]["prefix"]:
                origin_grouper[orig]["prefix"].append(pfx)
                origin_grouper[orig]["path"].append(path)

    origin_grouper["time"] = time
    return origin_grouper, origin_mapper
