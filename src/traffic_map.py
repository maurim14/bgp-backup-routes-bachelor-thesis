import logging as log
import diagrams
import os
import json

from diagrams import Diagram, Edge, Cluster
from diagrams.generic.network import Subnet, Router
from diagrams.custom import Custom

PATH_TRAFFIC_MAP_DATA = "data/traffic_map_data/"
PATH_TRAFFIC_MAP = "data/traffic_map/"
PATH_ROUTER_IMAGE = "data/router.png"
EDGE_COLOR_LIST = ["aqua", "pink", "olive", "brown","green", "blue", "yellow", "red", "black", "violet", "grey", "lightblue", "lightgreen", "auburn", "azure", "awesome"]

# also backup routes?
ALL_ROUTES = True

# display the same route more than once?
DOUBLE_ROUTES = False

def add_to_traffic_map(finding, meta_data):

    target = meta_data["target_as"]
    peer = meta_data["peer"]

    path = PATH_TRAFFIC_MAP_DATA + target
    if os.path.isfile(path):
        data_file = open(path, "r")
        data = data_file.read()
        if len(data) < 2:
            data = None
            data_file = open(path, "w")
        else:
            data = json.loads(data)
    else:
        data_file = open(path, "x")
        data = None


    backup_paths = []
    other_paths = []
    for find in finding["finding"]:
        backup_paths.append(find["backup-route"])
        tmp = []
        if find["post-route"] is not None:
            tmp.append(find["post-route"])
        if find["pre-route"] is not None:
            tmp.append(find["pre-route"])
        other_paths.append(tmp)

    if len(backup_paths) == 0:
        log.info("No routes found")
        return 0
    
    log.info("{} backup routes added".format(len(backup_paths)))
    log.info("{} other routes added".format(len(other_paths)))

    if data is None:
        data = {}
        data[peer] = {
                "backup-routes": backup_paths,
                "other-routes": other_paths,
                }
    else:
        if peer in data:
            [data[peer]["backup-routes"].append(x) for x in backup_paths if x not in data[peer]["backup-routes"]]

            [data[peer]["other-routes"].append(x) for x in other_paths if x not in data[peer]["other-routes"]]
        else:
            data[peer] = {
                    "backup-routes": backup_paths,
                    "other-routes": other_paths,
                    }
    os.remove(path)
    data_file = open(path, "w")
    data_file.write(json.dumps(data))


def traffic_map(meta_data, route_map):
    
    def node(name):
        return Custom(name, PATH_ROUTER_IMAGE)
    
    def edge(color):
        return Edge(color=color, style = "bold")
    
    def edge2(color):
        return Edge(color=color, style = "dashed")
    
    # only a single VP observed
    single = False
    target = meta_data["target_as"]

    path = PATH_TRAFFIC_MAP_DATA + target
    if os.path.isfile(path):
        data_file = open(path, "r")
        data = json.loads(data_file.read())
    else:
        log.error("Target has no found backup routes")

    with Diagram(name="Traffic Map for" + target, show = True):

        nodes = {}
        vps = list(data.keys())
        if route_map == "single":
            print("Select VP:")
            for vp in vps:
                print(" - {}".format(vp))
            
            select = input()
            if select not in vps:
                print("VP not available")
                exit()
            single = True
            vps = [select]


        with Cluster("AS of Interest", graph_attr = {"bgcolor": "#40e0d0"}):
            nodes[target] = node(target)

        with Cluster("Vantage Points"):
            for i in vps:
                nodes[i] = node(i)
        
        all_paths = []
        vp_counter = 0
        for vp in vps:
            if not single:
                color = EDGE_COLOR_LIST[vp_counter]
            for path in data[vp]["backup-routes"]:
                if single:
                    color = EDGE_COLOR_LIST[vp_counter]
                print(color, path)
                if path in all_paths and not DOUBLE_ROUTES:
                    if single:
                        vp_counter += 1
                    continue
                pth = path.split(" ")
                for autsys_index in range(len(pth) -1):
                    p1 = pth[autsys_index]
                    p2 = pth[autsys_index + 1]
                    if p1 not in nodes:
                        nodes[p1] = node(p1)
                    if p2 not in nodes:
                        nodes[p2] = node(p2)
                    nodes[p1] >> edge(color) >> nodes[p2]
                all_paths.append(path)
                if single:
                    vp_counter += 1

            if not single:
                vp_counter += 1

        vp_counter = 0
        for vp in vps:
            if not single:
                color = EDGE_COLOR_LIST[vp_counter]
            for path_ls in data[vp]["other-routes"]:
                if single:
                    color = EDGE_COLOR_LIST[vp_counter]
                for path in path_ls:
                    if path in all_paths and not DOUBLE_ROUTES:
                        continue
                    pth = path.split(" ")
                    for autsys_index in range(len(pth) -1):
                        p1 = pth[autsys_index]
                        p2 = pth[autsys_index + 1]
                        if p1 not in nodes:
                            nodes[p1] = node(p1)
                        if p2 not in nodes:
                            nodes[p2] = node(p2)
                        nodes[p1] >> edge2(color) >> nodes[p2]
                    all_paths.append(path)
                if single:
                    vp_counter += 1

            if not single:
                vp_counter += 1
