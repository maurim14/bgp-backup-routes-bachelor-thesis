import copy
import logging as log

from src.helper_functions import clean_as_path

FIXED_SIZE = 0
SECONDS_FOR_FULL_SWITCH = 10

def evaluate_announcements(
        stream,
        rib_data,
        bundle_size,
        ):
    """
    def database_checks(old_path):
        if origin in database:
            if path in database[origin]:
                switched_time = database[origin][path]["time"][0]

                if (elem.time - switched_time) > SECONDS_FOR_FULL_SWITCH:
                    reset = None
                    for timing in range(len(database[origin][path]["time"])):
                        if (elem.time - database[origin][path]["time"][timing]) < SECONDS_FOR_FULL_SWITCH:
                            reset = timing
                            break
                    if reset is not None:
                        database[origin][path]["time"] = database[origin][path]["time"][reset:]
                        database[origin][path]["prefix"] = database[origin][path]["prefix"][reset:]
                    else:
                        database[origin][path]["time"] = [elem.time]
                        database[origin][path]["prefix"] = [prefix]

                if prefix in database[origin][path]["prefix"]:
                    index = database[origin][path]["prefix"].index(prefix) + 1

                    database[origin][path]["prefix"] = database[origin][path]["prefix"][index:] + [prefix]
                    database[origin][path]["time"] = database[origin][path]["time"][index:] + [elem.time]
                else:
                    database[origin][path]["prefix"].append(prefix)
                    database[origin][path]["time"].append(elem.time)

            # path not in database[orig]
            else:
                database[origin][path] = {"prefix": [prefix], "time": [elem.time], "ex-path": old_path}

        # orig not in database
        else:
          database[origin] = {path: {"prefix": [prefix], "time": [elem.time], "ex-path": old_path}}
        
        if len(database[origin][path]["prefix"]) >= FIXED_SIZE:

            find = copy.deepcopy(database[origin][path])
            find["orig"] = copy.deepcopy(origin)
            find["path"] = copy.deepcopy(path)
            find["revert"] = False
            find["noise"] = False
            finding.append(find)

            counter_switches += 1

            ex_path = find["ex-path"]
            if not ex_path == path:
                if ex_path in origin_grouper[origin]["path"]:
                    find["noise"] = True

            if origin in sigma_time:
                if path not in sigma_time[origin]:
                    sigma_time[origin][path] = 0
                sigma_time[origin][sigma_time[origin]["path"]] += elem.time - sigma_time[origin]["time"]
                sigma_time[origin]["time"] = elem.time
                sigma_time[origin]["path"] = path
            else:
                sigma_time[origin] = {old_path: elem.time - time, "time": elem.time, path: 0, "path": path}

                key = len(finding) -1
                database[origin][path]["revert"] = key
                duration_base[origin] = {"path": path, "time": elem.time}

    def switch_detected():
        find = copy.deepcopy(database[origin][path])
        find["orig"] = copy.deepcopy(origin)
        find["path"] = copy.deepcopy(path)
        find["revert"] = False
        find["noise"] = False
        finding.append(find)

        counter_switches += 1

        ex_path = find["ex-path"]
        if not ex_path == path:
            if ex_path in origin_grouper[origin]["path"]:
                find["noise"] = True

        if origin in sigma_time:
            if path not in sigma_time[origin]:
                sigma_time[origin][path] = 0
            sigma_time[origin][sigma_time[origin]["path"]] += elem.time - sigma_time[origin]["time"]
            sigma_time[origin]["time"] = elem.time
            sigma_time[origin]["path"] = path
        else:
            sigma_time[origin] = {old_path: elem.time - time, "time": elem.time, path: 0, "path": path}

            key = len(finding) -1
            database[origin][path]["revert"] = key
            duration_base[origin] = {"path": path, "time": elem.time}
    """

### CODE START

    # init counter
    counter_updates = 0
    counter_aggregated_prefix_updates = 0
    counter_origin_changes = 0 
    counter_switches = 0
    counter_half_switches = 0

    # init data structures
    origin_grouper, origin_mapper = rib_data
    hog = copy.deepcopy(origin_grouper) # changed only when update comes, not on withdraw (for old path)
    time = origin_grouper.pop("time")
    duration_base = {}
    duration_finding = []
    finding = []
    sigma_time = {}
    database = {}
    active_backup = {}
    origin_changers = []

    # data checks
    for origin in origin_grouper:
        assert len(origin_grouper[origin]["prefix"]) == len(origin_grouper[origin]["path"])

    
    # Starting evaluation
    log.info("Evaluating Announcements")

    for elem in stream:

        assert type(elem.time) == float
        if elem.time < time:
            continue
        
        timetime_end = elem.time
        prefix = elem._maybe_field("prefix")
        counter_updates += 1

        if elem.type.lower() == "a":
            
            if "{" in elem._maybe_field("as-path"):
                counter_aggregated_prefix_updates += 1
                continue

            path = clean_as_path(elem._maybe_field("as-path"))
            origin = path.split(" ")[-1]

            # check if one prefix of backup route swapped back => end of backup route
            if origin in active_backup:
                for data_entry_index in range(len(active_backup[origin]["data-list"])):
                    data_entry = active_backup[origin]["data-list"][data_entry_index]
                    if prefix in data_entry["prefix-list"]:
                        if not path == data_entry["backup-route"]:
                            data_entry["uptime"] = elem.time - data_entry["uptime"]
                            data_entry["post-route"] = path
                            finding.append(copy.deepcopy(data_entry))
                            active_backup[origin]["data-list"].pop(data_entry_index)
                            counter_half_switches -= 1
                            counter_switches += 1
                            break
                else:
                    # check if prefix is announced on same path
                    for data_entry in active_backup[origin]["data-list"]:
                        if path == data_entry["backup-route"]:
                            data_entry["prefix-list"].append(prefix)
                            data_entry["time-list"].append(elem.time)
                            break

            if prefix in origin_mapper:
                should_origin = origin_mapper[prefix]
                
                # check if path not changed
                if prefix in origin_grouper[should_origin]["prefix"]:
                    index = origin_grouper[should_origin]["prefix"].index(prefix)
                    should_path = origin_grouper[should_origin]["path"][index]
                    if path == should_path:  
                        continue
            
                # path changed
                # pop old information of RIB
                index = origin_grouper[should_origin]["prefix"].index(prefix)
                old_path = origin_grouper[should_origin]["path"].pop(index)
                origin_grouper[should_origin]["prefix"].pop(index)
                
                # pop old information from RIB only updates
                history_index = hog[should_origin]["prefix"].index(prefix)
                hog[should_origin]["path"].pop(history_index)
                hog[should_origin]["prefix"].pop(history_index)

                origin_mapper[prefix] = origin

                # updating switch_database
                if should_origin in database:
                    if old_path in database[should_origin]:
                        if prefix in database[should_origin][old_path]["prefix"]:
                            index = database[should_origin][old_path]["prefix"].index(prefix)
                            if len(database[should_origin][old_path]["prefix"]) == 1:
                                if "revert" in database[should_origin][old_path].keys():
                                    finding[database[should_origin][old_path]["revert"]]["revert"] = True
                                database[should_origin].pop(old_path)
                            else:
                                database[should_origin][old_path]["prefix"].pop(index)
                                database[should_origin][old_path]["time"].pop(index)

                # update origin_grouper
                if origin in origin_grouper:
                    origin_grouper[origin]["prefix"].append(prefix)
                    origin_grouper[origin]["path"].append(path)
                else:
                    origin_grouper[origin] = {"prefix": [prefix], "path": [path]}

                # update history origin_grouper
                if origin in hog:
                    hog[origin]["prefix"].append(prefix)
                    hog[origin]["path"].append(path)
                else:
                    hog[origin] = {"prefix": [prefix], "path": [path]}

                # statistic
                if not origin == should_origin:
                    counter_origin_changes += 1
                    origin_changers.append(prefix)
                    continue

                if origin in database:
                    if path in database[origin]:
                        switched_time = database[origin][path]["time"][0]

                        if (elem.time - switched_time) > SECONDS_FOR_FULL_SWITCH:
                            reset = None
                            for timing in range(len(database[origin][path]["time"])):
                                if (elem.time - database[origin][path]["time"][timing]) < SECONDS_FOR_FULL_SWITCH:
                                    reset = timing
                                    break
                            if reset is not None:
                                database[origin][path]["time"] = database[origin][path]["time"][reset:]
                                database[origin][path]["prefix"] = database[origin][path]["prefix"][reset:]
                            else:
                                database[origin][path]["time"] = [elem.time]
                                database[origin][path]["prefix"] = [prefix]

                        if prefix in database[origin][path]["prefix"]:
                            index = database[origin][path]["prefix"].index(prefix) + 1

                            database[origin][path]["prefix"] = database[origin][path]["prefix"][index:] + [prefix]
                            database[origin][path]["time"] = database[origin][path]["time"][index:] + [elem.time]
                        else:
                            database[origin][path]["prefix"].append(prefix)
                            database[origin][path]["time"].append(elem.time)

                    # path not in database[orig]
                    else:
                        database[origin][path] = {"prefix": [prefix], "time": [elem.time], "ex-path": old_path}

                # orig not in database
                else:
                  database[origin] = {path: {"prefix": [prefix], "time": [elem.time], "ex-path": old_path}}
                
                if len(database[origin][path]["prefix"]) >= len(origin_grouper[origin]["prefix"]) * bundle_size:

                    noise = False
                    ex_path = database[origin][path]["ex-path"]
                    if not ex_path == path:
                        if ex_path in origin_grouper[origin]["path"]:
                            noise = True
                    
                    if origin in active_backup:

                        active_backup[origin]["data-list"].append(
                                    {

                                        "prefix-list": copy.deepcopy(database[origin][path]["prefix"]), 
                                        "backup-route": path,
                                        "pre-route": copy.deepcopy(database[origin][path]["ex-path"]),
                                        "post-route": None,
                                        "uptime": elem.time,
                                        "origin": origin,
                                        "time-list": copy.deepcopy(database[origin][path]["time"]),
                                        "noise": noise,
                                        "percent": len(origin_grouper[origin]["prefix"])
                                    }
                                    )
                    else:
                        active_backup[origin] = {
                                    "data-list": 
                                    [{
                                        "prefix-list": copy.deepcopy(database[origin][path]["prefix"]), 
                                        "backup-route": path,
                                        "pre-route": copy.deepcopy(database[origin][path]["ex-path"]),
                                        "post-route": None,
                                        "uptime": elem.time,
                                        "origin": origin,
                                        "time-list": copy.deepcopy(database[origin][path]["time"]),
                                        "noise": noise,
                                        "percent": len(origin_grouper[origin]["prefix"])
                                    }]
                                }

                    counter_half_switches += 1
            
            # pref not in origin_mapper
            else:
                origin_mapper[prefix] = origin

                if origin in origin_grouper:
                    origin_grouper[origin]["prefix"].append(prefix)
                    origin_grouper[origin]["path"].append(path)
                else:
                    origin_grouper[origin] = {"prefix": [prefix], "path": [path]}

                old_path = "?"
                if origin in hog:
                    if prefix in hog[origin]["prefix"]:
                        index = hog[origin]["prefix"].index(prefix)
                        old_path = hog[origin]["path"].pop(index)
                        hog[origin]["prefix"].pop(index)

                    hog[origin]["prefix"].append(prefix)
                    hog[origin]["path"].append(path)

                else:
                    hog[origin] = {"prefix": [prefix], "path": [path]}

                if origin in database:
                    if path in database[origin]:
                        switched_time = database[origin][path]["time"][0]

                        if (elem.time - switched_time) > SECONDS_FOR_FULL_SWITCH:
                            reset = None
                            for timing in range(len(database[origin][path]["time"])):
                                if (elem.time - database[origin][path]["time"][timing]) < SECONDS_FOR_FULL_SWITCH:
                                    reset = timing
                                    break
                            if reset is not None:
                                database[origin][path]["time"] = database[origin][path]["time"][reset:]
                                database[origin][path]["prefix"] = database[origin][path]["prefix"][reset:]
                            else:
                                database[origin][path]["time"] = [elem.time]
                                database[origin][path]["prefix"] = [prefix]

                        if prefix in database[origin][path]["prefix"]:
                            index = database[origin][path]["prefix"].index(prefix) + 1

                            database[origin][path]["prefix"] = database[origin][path]["prefix"][index:] + [prefix]
                            database[origin][path]["time"] = database[origin][path]["time"][index:] + [elem.time]
                        else:
                            database[origin][path]["prefix"].append(prefix)
                            database[origin][path]["time"].append(elem.time)

                    # path not in database[orig]
                    else:
                        database[origin][path] = {"prefix": [prefix], "time": [elem.time], "ex-path": old_path}

                # orig not in database
                else:
                  database[origin] = {path: {"prefix": [prefix], "time": [elem.time], "ex-path": old_path}}
                
                if len(database[origin][path]["prefix"]) >= len(origin_grouper[origin]["prefix"]) * bundle_size:
                    
                    noise = False
                    ex_path = database[origin][path]["ex-path"]
                    if not ex_path == path:
                        if ex_path in origin_grouper[origin]["path"]:
                            noise = True
                    
                    if origin in active_backup:

                        active_backup[origin]["data-list"].append(
                                    {

                                        "prefix-list": copy.deepcopy(database[origin][path]["prefix"]), 
                                        "backup-route": path,
                                        "pre-route": copy.deepcopy(database[origin][path]["ex-path"]),
                                        "post-route": None,
                                        "uptime": elem.time,
                                        "origin": origin,
                                        "time-list": copy.deepcopy(database[origin][path]["time"]),
                                        "noise": noise,
                                        "percent": len(origin_grouper[origin]["prefix"])
                                    }
                                    )
                    else:
                        active_backup[origin] = {
                                    "data-list": 
                                    [{
                                        "prefix-list": copy.deepcopy(database[origin][path]["prefix"]), 
                                        "backup-route": path,
                                        "pre-route": copy.deepcopy(database[origin][path]["ex-path"]),
                                        "post-route": None,
                                        "uptime": elem.time,
                                        "origin": origin,
                                        "time-list": copy.deepcopy(database[origin][path]["time"]),
                                        "noise": noise,
                                        "percent": len(origin_grouper[origin]["prefix"])
                                    }]
                                }

                    counter_half_switches += 1


        elif elem.type.lower() == "w":
            
            prefix = elem._maybe_field("prefix")
            try:
                path = clean_as_path(elem._maybe_field("as-path"))
                origin = path.split(" ")[-1]
            except:
                try:
                    origin = origin_mapper[prefix]
                except:
                    #counter_prefix_withdraw_without_announcement += 1
                    #time_prefix_withdraw_without_announcement.append(elem.time - time)
                    continue

            if origin in active_backup:
                for data_entry_index in range(len(active_backup[origin]["data-list"])):
                    data_entry = active_backup[origin]["data-list"][data_entry_index]
                    if prefix in data_entry["prefix-list"]:
                        data_entry["uptime"] = elem.time - data_entry["uptime"]
                        finding.append(active_backup[origin]["data-list"].pop(data_entry_index))
                        counter_switches += 1
                        break

            index = origin_grouper[origin]["prefix"].index(prefix)
            origin_grouper[origin]["prefix"].pop(index)
            old_path = origin_grouper[origin]["path"].pop(index)
            origin_mapper.pop(prefix)

            if origin in database:
                if old_path in database[origin]:
                    if prefix in database[origin][old_path]["prefix"]:
                        index = database[origin][old_path]["prefix"].index(prefix)
                        if len(database[origin][old_path]["prefix"]) == 1:
                            if "revert" in database[origin][old_path].keys():
                                finding[database[origin][old_path]["revert"]]["revert"] = True
                            database[origin].pop(old_path)
                        else:
                            database[origin][old_path]["prefix"].pop(index)
                            database[origin][old_path]["time"].pop(index)

        else:
            log.error("Unknown type {}".format(elem.type))

    half_finding = []
    for orig in active_backup:
        half_finding += active_backup[orig]["data-list"]

    log.info("{} evaluated announcements".format(counter_updates))
    log.info("{} switches found".format(counter_switches))
    log.info("{} half switches found".format(counter_half_switches))
    log.info("{} origin changes".format(counter_origin_changes))
    log.info("{} announcements used aggregation".format(counter_aggregated_prefix_updates))

    ret = {}
    
    ret["origin_changers"] = origin_changers
    ret["counter updates"] = counter_updates
    ret["counter aggregations"] = counter_aggregated_prefix_updates
    ret["half-finding"] = half_finding
    ret["finding"] = finding
    return ret
