import logging as log
import numpy as np
import seaborn as sns
import matplotlib as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns

from .. import storage
from .graph_helper import graph 

PATH_PROCESSED_DATA = "data/switches/" 

@graph
def two_prefixes_uptime(data):

    bundle_sizes = ["1.0", "0.5", "0.3"]
    dist_val = [[] for _ in range(len(bundle_sizes))]
    #dist_uptime = [[] for _ in range(len(bundle_sizes))]

    for elem in data:
            
            size = elem["bundle-size"]    
            if size not in bundle_sizes:
                continue

            if elem["post-route"] == "-":
                continue

            dist_val[bundle_sizes.index(size)].append(float(elem["uptime"])/len(elem["prefix-list"].split(" ")))
     #       dist_uptime[bundle_sizes.index(size)].append(float(elem["uptime"]))
    
    for i in range(len(dist_val)):
        dist_val[i] = sorted(dist_val[i])
    
    #for i in range(len(dist_uptime)):
    #    dist_uptime[i] = sorted(dist_uptime[i])
    
    fig, ax1 = plt.subplots(1, 1, sharex = False)

    for size_index in range(len(bundle_sizes)):
        ls = dist_val[size_index]
        fit = stats.norm.pdf(ls, np.mean(ls), np.std(ls))
        ax1.plot(ls,fit,'-o', label = bundle_sizes[size_index])
    
    ax1.set_xlabel("Uptime of backup case by amount of prefixes [uptime / #prefixes]")
    ax1.set_ylabel("Frequency")
    ax1.legend(loc="best", title="Bundle sizes:")
    ax1.set_xlim([-30, 100000])

    #for size_index in range(len(bundle_sizes)):
    #    ls = dist_uptime[size_index]
    #    ax2.hist(ls, 20, histtype='step', stacked=False, fill=False, label = bundle_sizes[size_index])
    #    #ax2.axhline(np.mean(ls), label = "mean for " + bundle_sizes[size_index])
    #ax2.set_xlabel("Uptime backup case [s]")
    #ax2.set_ylabel("Amount [#]")
    #ax2.legend(loc="best", title="Bundle sizes:")
    #ax2.set_yscale("log")
