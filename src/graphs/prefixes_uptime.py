import logging as log
import numpy as np
import seaborn as sns
import matplotlib as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns

from .. import storage

PATH_PROCESSED_DATA = "data/switches/" 

def prefixes_uptime(target):

    data = storage.load({}, PATH_PROCESSED_DATA ,static_name = "all_switches" + target)

    uptime_pref = []

    for vp in data["findings"]:

        for switch in vp:
            
            uptime_pref.append(switch["uptime"]/len(switch["prefix-list"]))
    
    xlabel = "Uptime/prefixes-amount [s]"
    ylabel = "Distribution"
    
    uptime_pref = sorted(uptime_pref)
    fit1 = stats.norm.pdf(uptime_pref, np.mean(uptime_pref), np.std(uptime_pref))  #this is a fitting indeed
    pl.plot(uptime_pref,fit1,'-o')

    
    # graph generation
    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    plt.show()
