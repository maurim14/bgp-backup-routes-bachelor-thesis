import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize

import datetime
from dateutil.parser import parse
 
@graph
def overall_update_compare(data):

    bundle_sizes = ["1.0", "0.5", "0.3"]
    dist = [[] for _ in range(len(bundle_sizes))]

    t1 = parse("2022-02-14").timestamp()
    t2 = parse("2022-02-21").timestamp()

    for elem in data:
        
        size = elem["bundle-size"]
        
        if size not in bundle_sizes:
            continue

        time = float(elem["time-list"].split(" ")[0])

        b = datetime.datetime.fromtimestamp(time)
        c = datetime.datetime(b.year, b.month, b.day)
        d = time - c.timestamp()

        dist[bundle_sizes.index(size)].append(d)

    for i in dist:
        sorted(i, reverse = True)

    plt.hist(dist, 70, histtype="step", stacked = True, fill=False, label=bundle_sizes)

    #plt.title("Distribution of detected backup routes over the day")
    plt.xlabel("time since midnight [s]")
    plt.yscale("log")
    plt.ylabel("Amount backup cases [#]")
    plt.legend(loc="upper left", title= "Bundle sizes:")
