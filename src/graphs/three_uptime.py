import logging as log
import numpy as np
import seaborn as sns
import matplotlib as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns

from .. import storage
from .graph_helper import graph 

PATH_PROCESSED_DATA = "data/switches/" 

color = ["tab:blue", "tab:orange", "tab:green"]

@graph
def three_uptime(data):

    bundle_sizes = ["1.0", "0.5", "0.3"]
    dist_uptime = [[] for _ in range(len(bundle_sizes))]

    for elem in data:
            
            size = elem["bundle-size"]    
            if size not in bundle_sizes:
                continue

            if elem["post-route"] == "-":
                continue

            dist_uptime[bundle_sizes.index(size)].append(float(elem["uptime"]))
    
    for i in range(len(dist_uptime)):
        dist_uptime[i] = sorted(dist_uptime[i])
    
    fig, ax = plt.subplots(1, 1, sharex = False)

    for size_index in range(len(bundle_sizes)):
        ls = dist_uptime[size_index]
        ax.hist(ls, 100, histtype='step', stacked=False, fill=False, label = bundle_sizes[size_index])
        #print(np.mean(ls))
        ax.axvline(np.mean(ls), label = "mean for " + bundle_sizes[size_index], color = color[size_index])
    ax.set_xlabel("Uptime backup case [s]")
    ax.set_ylabel("Amount [#]")
    ax.legend(loc="best", title="Bundle sizes:")
    ax.set_yscale("log")
