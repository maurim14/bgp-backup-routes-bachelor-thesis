import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse

@graph
def akamai_failure_compare(data):

    bundle_sizes = ["1.0", "0.5", "0.3"]
    dist2 = [[] for _ in range(len(bundle_sizes))]
    dist3 = [[] for _ in range(len(bundle_sizes))]

    t2 = parse("2021-05-02").timestamp()
    t3 = parse("2021-05-03").timestamp()

    for elem in data:
        
        size = elem["bundle-size"]
        
        if size not in bundle_sizes:
            continue
        
        time = float(elem["time-list"].split(" ")[0])

        if time >= t2 and time <= t2 + 86400:
            dist2[bundle_sizes.index(size)].append(1)

        elif time >= t3 and time <= t3 + 86400:
            dist3[bundle_sizes.index(size)].append(1)



    data2 = ["Before Outage", "Outage Day\n2021-05-03"]
    x_axis = np.arange(len(data2))

    fig = plt.figure()
    ax = fig.add_subplot(111)

    plt.bar(x_axis -0.3, [len(dist2[0]), len(dist3[0])], width=0.3, label = "bundle size: " + bundle_sizes[0])
    plt.bar(x_axis, [len(dist2[1]), len(dist3[1])], width=0.3, label = "bundle size: " + bundle_sizes[1])
    plt.bar(x_axis +0.3, [len(dist2[2]), len(dist3[2])], width=0.3, label = "bundle size: " + bundle_sizes[2])
    #plt.bar(x_axis +0.3, [len(dist[3]), len(dist2[3])], width=0.2, label = "bundle size: " + bundle_sizes[3])

    ax.set_xticks(x_axis)
    ax.set_xticklabels( data2 )
    
    plt.title("Updates for prefixes routed over DTAG \n before and at Akamai Outage (2021-05-03)")
    plt.ylabel("Amount Switches [# amount]")
    plt.legend(loc="upper left")
