import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse

color = ["tab:blue", "tab:orange", "tab:green"]

@graph
def four_uplink_changes(data):

    bundle_sizes = ["1.0", "0.3", "0.1"]
    dist = [[] for x in range(len(bundle_sizes))]
    count = [[0,0] for x in range(len(bundle_sizes))]

    for elem in data:
        
        size = elem["bundle-size"]
        if size not in bundle_sizes:
            continue

        pre = elem["pre-route"].split(" ")
        back = elem["backup-route"].split(" ")
        #pst = elem["post-route"]

        count[bundle_sizes.index(size)][0] += len(pre)   
        count[bundle_sizes.index(size)][1] += 1

        for cc in range(1, len(pre) + 1):
            if pre[0] == "?":
                break
            if len(back) == cc:
                break
            if not pre[-cc] == back[-cc]:
                dist[bundle_sizes.index(size)].append(cc )
                break

    fig, ax = plt.subplots(1, 1, sharex=True)

    mini = min(min(x) for x in dist)
    maxi = max(max(x) for x in dist)
    ax.hist(dist,bins = range(mini, maxi), histtype='step',align="left", density = True, stacked=False, fill=False, label = bundle_sizes)
    
    for size in range(len(bundle_sizes)):
        ax.axvline(count[size][0]/count[size][1], color = color[size], label = "mean AS path length: " + bundle_sizes[size])
    ax.set_ylabel("Relative frequency")
    ax.set_xlabel("First AS path change from origin AS [# Hops]")
    ax.legend(loc="best", title="Bundle size:")
