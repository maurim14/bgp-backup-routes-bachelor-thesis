import logging as log
import numpy as np
import seaborn as sns
import matplotlib as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns

from .. import storage

PATH_PROCESSED_DATA = "data/switches/" 

def time_diff_switches(target):

    data = storage.load({}, PATH_PROCESSED_DATA ,static_name = "all_switches" + target)

    data_ls = []

    for vp in data["findings"]:

        for switch in vp:
            
            data_ls.append(switch["time-list"][-1] - switch["time-list"][0])
    
    xlabel = "Time difference first and last announcement [s]"
    ylabel = "Distribution"
    
    data_ls = sorted(data_ls)
    fit1 = stats.norm.pdf(data_ls, np.mean(data_ls), np.std(data_ls))  #this is a fitting indeed
    pl.plot(data_ls,fit1,'-o')

    
    # graph generation
    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    plt.show()
