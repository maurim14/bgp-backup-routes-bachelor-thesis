import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize



@graph
def uptime_bundle_size(data):

    bundle_sizes = ["1.0", "0.5", "0.3"]
    dist = [[] for _ in range(len(bundle_sizes))]

    for elem in data:
        
        size = elem["bundle-size"]

        if size not in bundle_sizes:
            continue
        
        if elem["post-route"] == "-":
            continue

        uptime = float(elem["uptime"])
        
        dist[bundle_sizes.index(size)].append(uptime)

    for size_index in range(len(bundle_sizes)):
        size = bundle_sizes[size_index]
        data = dist[size_index]

        data = sorted(data)
        fit1 = stats.norm.pdf(data, np.mean(data), np.std(data))  #this is a fitting indeed
        pl.plot(data,fit1,'-o', label = "bundle size: " + size)


    plt.xlabel("uptime for switch [seconds]")
    plt.ylabel("distribution")
    plt.legend(loc="right")
