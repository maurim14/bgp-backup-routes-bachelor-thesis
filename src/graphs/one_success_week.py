import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse

@graph
def one_success_week(data):

    bundle_sizes = ["1.0", "0.5", "0.3", "0.1"]
    dist = []
    pref = []

    t2 = parse("2021-07-20").timestamp()
    t3 = parse("2021-07-27").timestamp()
    peers = ["1828", "35266"]

    for elem in data:
        
        size = elem["bundle-size"]
        if size not in bundle_sizes:
            continue

        if elem["peer"] not in peers:
            continue
        
        time = float(elem["time-list"].split(" ")[0])
        if time > t3:
            continue
        
        if time < t2:
            continue

        times = elem["time-list"].split(" ")
        time_mean = float(times[-1]) - float(times[0])

        #if time_mean > 100:
        #    continue
        
        dist.append(float(times[0]))
        pref += times

    change = 53200
    dist = [float(x) - t2 - change for x in dist]
    pref = [float(x) - t2 - change for x in pref]
    
    dist = sorted(dist, reverse = False)
    pref = sorted(pref, reverse = False)
    
    br = 0
    for i in range(len(dist)):
        if dist[i] > 0:
            br = i
            break

    maxi = max(dist)
    dist = dist[i + 1:] + [maxi + change + x for x in dist[:i]]

    br = 0
    for i in range(len(pref)):
        if pref[i] > 0:
            br = i
            break

    maxi = max(pref)
    pref = pref[i + 1:] + [maxi + 47200 + x for x in pref[:i]]

    dist.append(pref[-1])

    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)

    ax1.hist(dist, 7, histtype='step', stacked=False, fill=False, label = "24h")
    ax1.hist(dist, 42, histtype='step', stacked=False, fill=True, label = "4h")
    ax1.set_ylabel("Backup cases [#]")
    ax1.legend(loc="best", title="Time interval:")
    ax1.set_yscale("log")

    ax2.hist(pref, 7, histtype='step', stacked=False, fill=False, label = "24h")
    ax2.hist(pref, 42, histtype='step', stacked=False, fill=True, label = "4h")
    ax2.set_xlabel("Time since 2021-07-20 00:00:00 [s]")
    ax2.set_ylabel("Participating prefixes [#]")
    ax2.legend(loc="best", title="Time interval:")
    ax2.set_yscale("log")

    #ax1.title("Backup cases and affected prefixes routed over DTAG \n before, at and after Cloudflare uutage (2021-07-22)")
