import logging as log
import numpy as np
import seaborn as sns
import matplotlib as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns

from .. import storage

PATH_PROCESSED_DATA = "data/switches/" 
MAX_DISPLAYED = 100
def origin_count_dirty(target):

    data = storage.load({}, PATH_PROCESSED_DATA ,static_name = "all_switches" + target)

    origs = []
    counter = []
    pref = []

    for vp in data["findings"]:

        for switch in vp:
    
            if switch["origin"] in origs:
                counter[origs.index(switch["origin"])] += 1
            else:
                origs.append(switch["origin"])
                counter.append(1)
                pref.append(len(switch["prefix-list"]))

    data_ls = sorted([(origs[i],counter[i],pref[i]) for i in range(len(origs))], key = lambda y: y[1], reverse = True)

    data_ls = data_ls[:MAX_DISPLAYED]

    x,y,col = zip(*data_ls)

    x = list(x)
    y = list(y)
    col = list(col)

    color = []
    for i in col:
        if i == 1:
            color.append("red")
        if i <= 3:
            color.append("yellow")
        else:
            color.append("green")

    x = list(x)
    y = list(y)

    xlabel = "Origin"
    ylabel = "Detected Switches [# Amount]"
    
    plt.bar(x,y, color = color)


    plt.xticks(rotation=90, ha='right')

    
    # graph generation
    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    plt.show()
