import logging as log

from .. import storage

PATH_PROCESSED_DATA = "data/switches/" 

def backup_cases(target):

    data = storage.load({}, PATH_PROCESSED_DATA ,static_name = "all_switches" + target)

    count_switches = 0
    count_same_post_pre = 0
    count_case1 = 0
    count_case2 = 0
    count_case3 = 0

    for vp in data["findings"]:

        for switch in vp:
            
            count_switches += 1
            
            if switch["post-route"] == switch["pre-route"]:
                count_same_post_pre += 1
            
            path_bk = switch["backup-route"].split(" ")
            path_pre = switch["pre-route"].split(" ")
            path_post = switch["post-route"].split(" ")

            if target in path_bk and target not in path_pre and target not in path_post:
                count_case1 += 1

            elif target in path_pre and target in path_bk:
                count_case2 += 1

            elif path_bk[-1] == target:
                count_case3 += 1
    
    print("count_switches", count_switches)
    print("count_same_post_pre", count_same_post_pre/count_switches*100, "%")
    print("count_case1", count_case1/count_switches*100, "%")
    print("count_case2", count_case2/count_switches*100, "%")
    print("count_case3", count_case3/count_switches*100, "%")
