import logging as log
from .. import storage
import os
import inspect
import datetime
import matplotlib as mpl
import matplotlib.pyplot as plt

PATH_PROCESSED_DATA = "data/switches/"
STATIC_NAME = "strong_dataset"
PATH_IMAGES = "graphical_output/"

def specific(func):
    def inner():
        log.debug("Decorating graph function")
 
        meta = {}



        mpl.rcParams['font.size'] = 14
        mpl.rcParams['figure.dpi'] = 100
        mpl.rcParams["figure.figsize"] = (15,5)

        func(data)

        #base_dir = inspect.stack()[1].function
        base_dir = func.__name__
        print(base_dir)
        file_name = base_dir + "/" + str(datetime.datetime.now())

        log.info("Storing file {}".format(file_name))

        if not os.path.isdir(PATH_IMAGES + base_dir):
            os.mkdir(PATH_IMAGES + base_dir)

        plt.savefig(PATH_IMAGES + file_name + ".png")
        plt.show()
    return inner

def graph(func):
    def inner():
        log.debug("Decorating graph function")
        
        data = storage.load({}, PATH_PROCESSED_DATA ,static_name = STATIC_NAME, csv_file = True)

        mpl.rcParams['font.size'] = 14
        mpl.rcParams['figure.dpi'] = 100
        mpl.rcParams["figure.figsize"] = (15,5)

        func(data)

        #base_dir = inspect.stack()[1].function
        base_dir = func.__name__
        print(base_dir)
        file_name = base_dir + "/" + str(datetime.datetime.now())

        log.info("Storing file {}".format(file_name))

        if not os.path.isdir(PATH_IMAGES + base_dir):
            os.mkdir(PATH_IMAGES + base_dir)

        plt.savefig(PATH_IMAGES + file_name + ".png")
        plt.show()
    return inner

def graph_intro():
    
    data = storage.load({}, PATH_PROCESSED_DATA ,static_name = STATIC_NAME, csv_file = True)
    return data

def graph_finalize():
    
    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)

    base_dir = inspect.stack()[1].function
    file_name = base_dir + "/" + str(datetime.datetime.now())

    if not os.path.isdir(PATH_IMAGES + base_dir):
        os.mkdir(PATH_IMAGES + base_dir)

    plt.savefig(PATH_IMAGES + file_name + ".png")
    plt.show()
