import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse

@graph
def specific_bundle_size_amount(data):

    bundle_sizes = ["1.0", "0.85", "0.7", "0.5", "0.4", "0.3", "0.2", "0.1", "0.05"]
    dist2 = [0 for _ in range(len(bundle_sizes))]
    dist3 = [0 for _ in range(len(bundle_sizes))]

    t2 = parse("2022-02-14").timestamp()
    t3 = parse("2022-02-15").timestamp()

    for elem in data:
        
        size = elem["bundle-size"]
        
        if size not in bundle_sizes:
            continue
        
        time = float(elem["time-list"].split(" ")[0])

        if time >= t2 and time <= t2 + 86400:
            dist2[bundle_sizes.index(size)] += 1

            if elem["noise"] == "True":
                dist3[bundle_sizes.index(size)] += 1

    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    x_axis = list(reversed(np.arange(len(bundle_sizes))))
    #dist2 =  list(reversed(dist2))

    plt.bar(x_axis, dist2, label = "Detected backup cases")
    #plt.bar(x_axis - 0.2, dist2, width=0.5, label = "Newly learned routes")

    plt.xticks(x_axis, bundle_sizes)
    plt.ylabel("Amount [#]")
    plt.xlabel("Bundle sizes")
