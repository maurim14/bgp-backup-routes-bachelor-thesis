import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse

@graph
def decix_prefix_times(data):

    bundle_sizes = ["1.0", "0.5", "0.3"]
    dist2 = []
    dist3 = []

    t2 = parse("2018-04-09").timestamp()
    t3 = parse("2018-04-10").timestamp()

    for elem in data:
        
        size = elem["bundle-size"]
        
        time = float(elem["time-list"].split(" ")[0])

        if time >= t2 and time <= t2 + 86400:
            #dist2 += elem["time-list"].split(" ")
            dist2.append(time)

        elif time >= t3 and time <= t3 + 86400:
            #dist3 += elem["time-list"].split(" ")
            dist3.append(time)

    dist2 = sorted(dist2, reverse = True)
    dist3 = sorted(dist3, reverse = True)

    dist2 = [float(x) - t2 for x in dist2]
    dist3 = [float(x) - t3 for x in dist3]


    data2 = ["Outage Day\n2022-04-09", "Day After Outage"]
    #data2 = ["Before Outage", "Outage Day"]
    plt.hist([dist2, dist3], 100, histtype='step', stacked=False, fill=False, label = data2)
   
    plt.title("Updates for prefixes routed over DTAG \n after and at decix outage")
    plt.xlabel("Announcement arrival over the day [seconds from 00:00:00]")
    plt.ylabel("Amount backup cases [# amount]")
    #plt.yscale("log")
    plt.legend(loc="upper left")
