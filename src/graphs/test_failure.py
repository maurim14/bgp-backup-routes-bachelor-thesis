import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize

from dateutil.parser import parse

@graph
def amsix_failure_prefix_times(data):

    bundle_sizes = ["1.0", "0.5", "0.3"]
    dist = []
    dist2 = []
    dist3 = []
    dist4 = []
    dist5 = []
    dist6 = []

    t2 = parse("2015-05-13").timestamp()
    t3 = parse("2019-05-13").timestamp()
    t4 = parse("2020-05-13").timestamp()
    t5 = parse("2021-05-13").timestamp()
    t6 = parse("2022-05-13").timestamp()

    for elem in data:
        
        size = elem["bundle-size"]
        
        if not size == "0.3":
            continue
        
        time = float(elem["time-list"].split(" ")[0])

        if time < t2:
            dist += elem["time-list"].split(" ")

        elif time >= t2 and time <= t2 +86400:
            dist2 += elem["time-list"].split(" ")

        elif time >= t3 and time <= t3 + 86400:
            dist3 += elem["time-list"].split(" ")

        elif time >= t4 and time <= t4 + 86400:
            dist4 += elem["time-list"].split(" ")

        elif time >= t5 and time <= t5 + 86400:
            dist5 += elem["time-list"].split(" ")

        elif time >= t5 and time <= t5 + 86400:
            dist5 += elem["time-list"].split(" ")

    dist = sorted(dist, reverse = True)
    dist2 = sorted(dist2, reverse = True)
    dist3 = sorted(dist3, reverse = True)
    dist4 = sorted(dist4, reverse = True)
    dist5 = sorted(dist5, reverse = True)
    dist6 = sorted(dist6, reverse = True)

    dist = [float(x) - 1431216000 for x in dist]
    dist2 = [float(x) - t2 for x in dist2]
    dist3 = [float(x) - t3 for x in dist3]
    dist4 = [float(x) - t4 for x in dist4]
    dist5 = [float(x) - t5 for x in dist5]
    dist6 = [float(x) - t6 for x in dist6]

    data2 = ["Before Outage", "Outage Day\n2015-05-13", "2019-05-13", "2020-05-13", "2021-05-13", "2022-05-13"]
    #data2 = ["Before Outage", "Outage Day"]
    plt.hist([dist, dist2, dist3, dist4, dist5, dist6], 100, histtype='step', stacked=True, fill=False, label = data2)
   
    plt.title("Updates for prefixes routed over DTAG \n before and at AMSIX Outage (2015-05-13)")
    plt.xlabel("Announcement arrival over the day [seconds from 00:00:00]")
    plt.ylabel("Path Updates [# amount]")
    plt.legend(loc="upper left")
