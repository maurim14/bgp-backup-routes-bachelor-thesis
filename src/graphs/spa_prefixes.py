import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize

@graph
def spa_prefixes(data):

    spa_switch = []
    dpa_switch = []
    rest = []

    for elem in data:
        
        if not elem["bundle-size"] == "1.0":
            continue
        
        if elem["post-route"] == "-":
            continue

        pref_ls = elem["prefix-list"].split(" ")
        prefs = len(pref_ls)
        if prefs == 1:
            spa_switch.append(elem)
        elif prefs == 2:
            dpa_switch.append(elem)
        else:
            rest.append(elem)
    
    a = len(spa_switch)
    b = len(dpa_switch)
    c = len(rest)
    
    plt.bar(["prefix amount == 1", "prefix amount == 2", "prefix amount > 2"], [a,b,c])

    plt.ylabel("Amount switches")
