import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse

@graph
def cloudflare_failure_prefix_times(data):

    bundle_sizes = ["1.0", "0.5", "0.3"]
    bundle_sizes = ["0.1"]
    dist2 = []
    dist3 = []

    #t2 = parse("2021-07-22").timestamp()
    #t3 = parse("2021-07-21").timestamp()
    t2 = parse("2020-07-17").timestamp()
    t3 = parse("2020-07-18").timestamp()

    for elem in data:
        
        size = elem["bundle-size"]
        
        #if not size == "0.3":
        #    continue
        
        time = float(elem["time-list"].split(" ")[0])
        rrc = elem["route-collector"]

        if time >= t2 and time <= t2 +86400:
            if rrc == "rrc11":
                dist2 += elem["time-list"].split(" ")

            if rrc == "rrc14":
                dist3 += elem["time-list"].split(" ")

        #elif time >= t3 and time <= t3 + 86400:
        #    dist3 += elem["time-list"].split(" ")

    dist2 = sorted(dist2, reverse = True)
    dist3 = sorted(dist3, reverse = True)

    dist2 = [float(x) - t2 for x in dist2]
    dist3 = [float(x) - t2 for x in dist3]

    data2 = ["Before Outage", "Outage Day\n2021-07-22"]
    #data2 = ["Before Outage", "Outage Day"]
    plt.hist([dist2, dist3], 100, histtype='step', stacked=True, fill=False, label = data2)
   
    plt.title("Updates for prefixes routed over DTAG \n before and at Cloudflare Outage (2021-07-22)")
    plt.xlabel("Announcement arrival over the day [seconds from 00:00:00]")
    plt.ylabel("Amount backup cases [# amount]")
    plt.yscale("log")
    plt.legend(loc="upper left")
