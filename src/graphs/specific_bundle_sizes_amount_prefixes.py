import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse

@graph
def specific_bundle_sizes_amount_prefixes(data):

    bundle_sizes = {}

    t1 = parse("2022-02-14").timestamp()
    t2 = parse("2022-02-15").timestamp()

    for elem in data:

        time = float(elem["time-list"].split(" ")[0])
        if t1 >= time or time >= t2:
            continue
        
        if not elem["peer"] == "1299" or elem["peer"] == "57695" or elem["peer"] == "8896" or elem["peer"] == "8218":
            continue

        size = elem["bundle-size"]
        if size in bundle_sizes:
            bundle_sizes[size].append(len(elem["prefix-list"]))
        else:
            bundle_sizes[size] = [len(elem["prefix-list"])]
       
    for size in bundle_sizes:
        data = bundle_sizes[size]

        data = sorted(data)
        fit1 = stats.norm.pdf(data, np.mean(data), np.std(data))  #this is a fitting indeed
        pl.plot(data,fit1,'-o', label = "bundle size: " + size)

    #plt.xlim([-30,750])
    plt.xlabel("prefixes used in switch [# amount]")
    plt.ylabel("distribution")
    plt.legend(loc="right")
