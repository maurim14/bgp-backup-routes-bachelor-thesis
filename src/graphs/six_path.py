import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse

@graph
def five_path_len(data):

    bundle_sizes = ["1.0", "0.3", "0.1"]
    dist = [[] for x in range(len(bundle_sizes))]

    count_backups, count_full_backup = 0, 0

    for elem in data:
        
        size = elem["bundle-size"]
        if size not in bundle_sizes:
            continue

        pre = elem["pre-route"].split(" ")
        back = elem["backup-route"].split(" ")
        #pst = elem["post-route"]

        if pre[0] == "?":
            continue

        cc = len(back) - len(pre)
        dist[bundle_sizes.index(size)].append(cc)

    fig, ax = plt.subplots(1, 1, sharex=True)

    mini = min(min(x) for x in dist)
    maxi = max(max(x) for x in dist)
    ax.hist(dist,bins = range(mini, maxi),align="left", histtype='step', density = True, stacked=False, fill=False, label = bundle_sizes)
    ax.set_ylabel("relative frequency")
    ax.set_xlabel("AS path length difference: len(backup) - len(active) [Hops]")
    ax.legend(loc="best", title="Bundle size:")
