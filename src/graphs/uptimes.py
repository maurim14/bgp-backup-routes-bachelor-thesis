import logging as log
import numpy
import seaborn as sns
import matplotlib as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns

from .. import storage

PATH_PROCESSED_DATA = "data/switches/" 

def uptimes(target):

    data = storage.load({}, PATH_PROCESSED_DATA ,static_name = "all_switches" + target)

    uptimes = []

    for vp in data["findings"]:

        for switch in vp:
            
            uptimes.append(switch["uptime"])
    
    xlabel = "Uptime backup route [s]"
    ylabel = "CDF"
    ax = sns.ecdfplot(data=numpy.array(uptimes))
    
    ax.set_ylim([0.5, None])
    # graph generation
    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    plt.show()
