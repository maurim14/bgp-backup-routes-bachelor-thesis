import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse
import inspect
import datetime
import os

PATH_IMAGES = "graphical_output/"
PATH_SWITCHES = "data/switches/"

def multiple_vs_once_measurement():

    log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
    plt.set_loglevel("info") 
    peers = ["1299", "57695"]

    week = []
    days = []

    for peer in peers:

        # one week measurement
        meta = {}
        meta["start"] = "2022-02-14 00:00:00"
        meta["end"] = "2022-02-21 00:00:00"
        meta["peer"] = peer
        meta["collector"] = "rrc01"
        meta["singlefile"] = "False"
        meta["target_as"] = "3320"
        meta["bundle size"] = "1.0"
        tmp = storage.load(meta, PATH_SWITCHES)
        week.append(tmp)

        for date_day in range(14, 21, 1):
            date_start = "2022-02-{} 00:00:00".format(date_day)
            date_end = "2022-02-{} 00:00:00".format(date_day + 1)
            print(date_start, date_end)

            meta = {}
            meta["start"] = date_start
            meta["end"] = date_end
            meta["peer"] = peer
            meta["collector"] = "rrc01"
            meta["singlefile"] = "False"
            meta["target_as"] = "3320"
            meta["bundle size"] = "1.0"
            days.append(storage.load(meta, PATH_SWITCHES))

    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)

    t2 = parse("2022-02-14").timestamp()

    week_data = []
    for data in week:
        for elem in data["finding"]:
            
            time = float(elem["time-list"][0])
            week_data.append(time - t2)

    days_data = []
    for data in days:
        for elem in data["finding"]:
            
            time = float(elem["time-list"][0])
            days_data.append(time - t2)

    days_data = sorted(days_data, reverse = True)
    week_data = sorted(week_data, reverse = True)

    #days_data = [float(x) - t2 for x in days_data]
    #week_data = [float(x) - t for x in week_data]

    data2 = [str(x//4) if x%4==0 else "" for x in range(0,7)]
    data2 = [x for x in range(0,7)]

    fig, ax = plt.subplots()

    plt.hist([days_data, week_data], bins = 7, histtype='bar', stacked=False, fill=True, label = ["multiple measurments", "single measurement"])
    #plt.hist([days_data, week_data], bins=np.arange(28)-0.5, histtype='step', stacked=False, fill=False, label = ["multiple measurments", "single measurement"])
    
    plt.xticks([x*86400 for x in range(7)], data2)

    #ax.set_xticks(range(28))
    #ax.set_xticklabels(data2)
  
    plt.title("Test")
    plt.xlabel("Days from week begin [d]")
    plt.ylabel("Occurences of switches [#]")
    plt.legend(loc="upper left")
    plt.xlim([0, 604800])
    
    base_dir = inspect.stack()[0].function
    #base_dir = func.__name__
    print(base_dir)
    file_name = base_dir + "/" + str(datetime.datetime.now())

    log.info("Storing file {}".format(file_name))

    if not os.path.isdir(PATH_IMAGES + base_dir):
        os.mkdir(PATH_IMAGES + base_dir)

    plt.savefig(PATH_IMAGES + file_name + ".png")
    plt.show()
