import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import pylab as pl

from .. import storage
from .graph_helper import graph

PATH_SPA_CMP = "data/spa_compare/"

@graph
def spa_announcement_compare(dart):

    full_data = storage.load({}, PATH_SPA_CMP, static_name = "spa_compare")

    xlabel="Announcements [#]"
    ylabel="Relative frequency"
    
    full_data["rest"] = sorted(full_data["rest"], reverse = True)

    fit1 = stats.norm.pdf(full_data["rest"], np.mean(full_data["rest"]), np.std(full_data["rest"]))  #this is a fitting indeed
    pl.plot(full_data["rest"],fit1,'-o', label="all prefixes")

    for bundle_size in full_data["spa"].keys():
        
        full_data["spa"][bundle_size] = sorted(full_data["spa"][bundle_size], reverse = True)

        fit2 = stats.norm.pdf(full_data["spa"][bundle_size], np.mean(full_data["spa"][bundle_size]), np.std(full_data["spa"][bundle_size]))  #this is a fitting indeed
        pl.plot(full_data["spa"][bundle_size],fit2,'-o', label="spa backup case ({})".format(bundle_size))

    # graph generation
    plt.title("Compare activity of single prefix AS (SPA) prefixes against all other prefixes")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xlim([-5,30])
    plt.legend(loc='right')
