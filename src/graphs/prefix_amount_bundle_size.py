import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize

@graph
def prefix_amount_bundle_size(data):

    bundle_sizes = ["1.0", "0.7", "0.5", "0.3"]
    dist = [[] for _ in range(len(bundle_sizes))]

    for elem in data:
        
        size = elem["bundle-size"]

        if size not in bundle_sizes:
            continue
        
        if elem["post-route"] == "-":
            continue

        pref_ls = elem["prefix-list"].split(" ")
        prefs = len(pref_ls)
        
        dist[bundle_sizes.index(size)].append(prefs)

    for size_index in range(len(bundle_sizes)):
        size = bundle_sizes[size_index]
        data = dist[size_index]

        data = sorted(data)
        fit1 = stats.norm.pdf(data, np.mean(data), np.std(data))  #this is a fitting indeed
        pl.plot(data,fit1,'-o', label = size)

    plt.title("Distribution")
    plt.xlim([-30,500])
    plt.xlabel("prefixes used in switch [#]")
    plt.ylabel("frequency")
    plt.legend(loc="right", title="Bundle size:")
