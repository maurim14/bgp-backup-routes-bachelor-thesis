import logging as log
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib as mpl
import scipy.stats as stats
import pylab as pl
import seaborn as sns
import ast

from .. import storage
from .graph_helper import graph 
#from .graph_helper import graph_intro, graph_finalize
from dateutil.parser import parse

@graph
def amsix_failure_compare(data):

    bundle_sizes = ["1.0", "0.5", "0.3"]
    dist = [[] for _ in range(len(bundle_sizes))]
    dist2 = [[] for _ in range(len(bundle_sizes))]
    dist3 = [[] for _ in range(len(bundle_sizes))]
    dist4 = [[] for _ in range(len(bundle_sizes))]
    dist5 = [[] for _ in range(len(bundle_sizes))]
    dist6 = [[] for _ in range(len(bundle_sizes))]


    t2 = parse("2015-05-13").timestamp()
    t3 = parse("2019-05-13").timestamp()
    t4 = parse("2020-05-13").timestamp()
    t5 = parse("2021-05-13").timestamp()
    t6 = parse("2022-05-13").timestamp()

    for elem in data:
        
        size = elem["bundle-size"]
        
        if size not in bundle_sizes:
            continue
        
        time = float(elem["time-list"].split(" ")[0])

        if time < int("1431475200"):
            dist[bundle_sizes.index(size)].append(1)

        elif time >= t2 and time <= t2 + 86400:
            dist2[bundle_sizes.index(size)].append(1)

        elif time >= t3 and time <= t3 + 86400:
            dist3[bundle_sizes.index(size)].append(1)

        elif time >= t4 and time <= t4 + 86400:
            dist4[bundle_sizes.index(size)].append(1)

        elif time >= t5 and time <= t5 + 86400:
            dist5[bundle_sizes.index(size)].append(1)

        elif time >= t6 and time <= t6 + 86400:
            dist6[bundle_sizes.index(size)].append(1)



    data2 = ["2015-05-12", "Outage Day\n2015-05-13", "2019-05-13", "2020-05-13", "2021-05-13", "2022-05-13"]
    x_axis = np.arange(len(data2))

    fig = plt.figure()
    ax = fig.add_subplot(111)

    plt.bar(x_axis -0.3, [len(dist[0]), len(dist2[0]), len(dist3[0]), len(dist4[0]), len(dist5[0]), len(dist6[0])], width=0.3, label = bundle_sizes[0])
    
    plt.bar(x_axis, [len(dist[1]), len(dist2[1]), len(dist3[1]), len(dist4[1]), len(dist5[1]), len(dist6[1])], width=0.3, label = bundle_sizes[1])
    
    plt.bar(x_axis +0.3, [len(dist[2]), len(dist2[2]), len(dist3[2]), len(dist4[2]), len(dist5[2]), len(dist6[2])], width=0.3, label = bundle_sizes[2])
    #plt.bar(x_axis +0.3, [len(dist[3]), len(dist2[3])], width=0.2, label = bundle_sizes[3])

    ax.set_xticks(x_axis)
    ax.set_xticklabels( data2 )
    
    plt.title("NTT+UNITAS Updates for prefixes routed over DTAG \n before and at AMSIX Outage (2015-05-13)")
    plt.ylabel("Amount switches [#]")
    plt.legend(loc="upper left", title= "Bundle sizes:")
