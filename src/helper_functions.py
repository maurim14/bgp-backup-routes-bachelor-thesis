def clean_as_path(path):
    splitted = path.split(" ")
    path_list = []
    for asn in splitted:
        if asn not in path_list:
            path_list.append(asn)
    return " ".join(path_list)


def strong_dataset(finding, meta_data):

    half_find = finding["half-finding"]
    find = finding["finding"]

    new_full = []
    new_half = []
    add_info = {}
    add_info["vp"] = meta_data["peer"]
    add_info["collector"] = meta_data["collector"]
    add_info["bundle_size"] = meta_data["bundle size"]
    add_info["target_as"] = meta_data["target_as"]

    for switch in find:
        
        full = {**switch, **add_info}
        full["post-route"] = full["post-route"].replace('"','')
        full["pre-route"] = full["pre-route"].replace('"','')
        full["backup-route"] = full["backup-route"].replace('"','')
        full["prefix-list"] = ' '.join(full["prefix-list"])
        full["time-list"] = ' '.join([str(x) for x in full["time-list"]])
        new_full.append(full)

    for switch in half_find:

        full = {**switch, **add_info}
        full["post-route"] = "-"
        full["pre-route"] = full["pre-route"].replace('"','')
        full["backup-route"] = full["backup-route"].replace('"','')
        full["time-list"] = ' '.join([str(x) for x in full["time-list"]])
        full["prefix-list"] = ' '.join(full["prefix-list"])
        new_half.append(full)

    return {"findings": new_full, "half-findings": new_half}
