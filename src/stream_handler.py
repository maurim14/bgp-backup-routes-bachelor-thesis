import pybgpstream
import random
import datetime
import logging as log

from src import storage

from dateutil.parser import parse

DATA_PATH = "data/ribs/"

class StreamHandler:

    def __init__(self, target_as: int, time_start: str, time_end: str, collector: str, peer: str = None):
        self.time_start = time_start
        self.time_end = time_end
        self.collector = collector
        self.peer = str(peer) if peer is not None else self.get_random_peer()
        self.target = target_as
        self.rib_start = str(datetime.datetime.fromtimestamp(parse(self.time_start).timestamp() - 300))
        self.rib_end = str(datetime.datetime.fromtimestamp(parse(self.time_start).timestamp() + 300))

        log.info("Creating Stream object")
        log.debug("time_start {}".format(self.time_start))
        log.debug("time_end {}".format(self.time_end))
        log.debug("collector {}".format(self.collector))
        log.debug("peer {}".format(self.peer))
        log.debug("target AS {}".format(self.target))

    def get_random_peer(self):

        log.debug("choosing random peer")
        until_time = str(datetime.datetime.fromtimestamp(parse(self.time_start).timestamp() + 60))
        stream = pybgpstream.BGPStream(
                                       from_time = self.time_start,
                                       collector = self.collector,
                                       until_time = until_time,
                                       record_type = "updates",
                                       )
        asn = []
        for elem in stream:
            if elem.peer_asn not in asn:
                asn.append(elem.peer_asn)

        return asn[random.randint(0,len(asn) - 1)]


    def collect_data_updates(self):

        log.info("loading remote data - announcements")
        if self.target is not None:
            stream = pybgpstream.BGPStream(
                                             from_time = self.time_start,
                                             until_time = self.time_end,
                                             collector = self.collector,
                                             record_type = "updates",
                                             filter = "peer " + str(self.peer) + " and path _" + self.target + "_",
                                             )
        else:
            stream = pybgpstream.BGPStream(
                                             from_time = self.time_start,
                                             until_time = self.time_end,
                                             collector = self.collector,
                                             record_type = "updates",
                                             filter = "peer " + str(self.peer),
                                             )
        self.data = stream
        return True

    def collect_data_rib(self):
        log.info("loading remote data - rib")
        stream = pybgpstream.BGPStream(
                                         from_time = self.rib_start,
                                         until_time = self.rib_end,
                                         collector = self.collector,
                                         record_type = "ribs",
                                         filter = "peer " + str(self.peer),
                                         )
        self.rib_data = stream
        return True

    def meta_data(self):
        meta = {}
        meta["time_start"] = self.time_start
        meta["time_end"] = self.time_end
        meta["collector"] = self.collector
        meta["peer"] = self.peer
        meta["target_as"] = self.target
        meta["rib_start"] = self.rib_start
        meta["rib_end"] = self.rib_end
        return meta

    def store_data(self):
        
        meta_data = self.meta_data()
        return storage.store(self.rib_data, meta_data, DATA_PATH)

    def load_data(self):

        meta_data = self.meta_data()
        self.rib_data = storage.load(meta_data, DATA_PATH)
        return self.rib_data
