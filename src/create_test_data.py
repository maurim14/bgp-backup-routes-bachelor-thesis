import random

#from src.static_variables import TEST_NUMBER_AS, TEST_NUMBER_PREF, TEST_NUMBER_ANN

#NUMBER_AS = TEST_NUMBER_AS
#NUMBER_PREF = TEST_NUMBER_PREF
#NUMBER_ANN = TEST_NUMBER_ANN

NUMBER_AS = 10
NUMBER_PREF = 15
NUMBER_ANN = 10

def create_test(filter_asn = False):
  
  asn = [str(x + 1) for x in range(NUMBER_AS)]
  prefs = [str(x+1) + ".0.0.0/8" for x in range(NUMBER_PREF)]

  asn_orig = {}
  for i in prefs[10:]:
    x = random.randint(2,NUMBER_AS)
    if asn[int(x)-1] in asn_orig:
      asn_orig[asn[int(x)-1]].append(i)
    else:
      asn_orig[asn[int(x)-1]] = [i]

  typ = "a"

  asn_orig["1"] = [prefs[0], prefs[1], prefs[2], prefs[3], prefs[4], prefs[5], prefs[6],prefs[7], prefs[8],prefs[9]]

  paths = {}
  for i in range(NUMBER_AS):

    x = random.randint(2, 5)
    # path amount
    pathes = []
    for _ in range(x):
      path = []
      q = random.randint(1,3)
      for _ in range(q):
        p = random.randint(1, NUMBER_AS)
        if str(p) == asn[i] or str(p) in path:
          continue
        else:
          path.append(str(p))
      path.insert(0, str(0))
      path.append(asn[i])
      tmp =" ".join(path)
      pathes.append(tmp)
    paths[asn[i]] = pathes

  ann = []
  for i in range(NUMBER_ANN):
    time = i
    x = random.randint(0, NUMBER_AS -1)
    if asn[x] in asn_orig:
      t = random.randint(0, len(paths[asn[x]]) -1)
      path = paths[asn[x]][t]
      t = random.randint(0, len(asn_orig[asn[x]])-1)
      pref = asn_orig[asn[x]][t]
      
      if filter_asn:
        if filter_asn == path.split(" ")[-1]:
            ann.append(Elem(typ, time, pref, path))
      else:
        ann.append(Elem(typ, time, pref, path))
    else:
      continue

  #special = [Elem(typ, 10, "1.0.0.0/8", "0 2 1"), Elem(typ, 10, "2.0.0.0/8", "0 2 1"), Elem(typ, 2000, "1.0.0.0/8", "0 3 1"), Elem(typ, 2000, "2.0.0.0/8", "0 3 1")]

  return ann #+ special
    

class Elem:
  def __init__(self, typ, time, pref, path):
    self.time = float(time)
    self.type = typ
    self.fields = {}
    self.fields["as-path"] = path
    self.fields["prefix"] = pref

  def _maybe_field(self, field):
    if field == "as-path":
      return self.fields["as-path"]
    elif field == "prefix":
      return self.fields["prefix"]
    else:
      return "ERROR"

  def show(self):
    print(self.type," | " ,self.time, " | ", self.fields["prefix"], " | ", self.fields["as-path"])
