import logging as log
import hashlib
import json
import os
import csv

def create_name(meta_data: dict):
    meta_data_str = str(meta_data) #.replace("\'","\"")
    log.info("Hashing {}".format(meta_data_str))
    m = hashlib.sha256()
    m.update(json.dumps(meta_data).encode("ascii"))
    name = str(m.hexdigest())
        
    log.info("Hash Output {}".format(name))
    return name

def store(data, meta_data: dict, path: str, static_name = None, csv_file = False):

    if static_name is not None:
        log.info("Storing file content with static name {}".format(static_name))
        name = static_name
        path = path + name
    else:
        name = create_name(meta_data)
        path = path + name

    file_exists = False
    if os.path.isfile(path):
            log.info("Data already locally stored")
            file_exists = True
            if not csv_file:
                if static_name is not None:
                    log.warning("Replacing existing static_name file")
                    os.remove(path)
                else:
                    return 0

    log.info("Storing data {}".format(name))
    with open(path, "a", newline='') as data_file:
        if csv_file:
            log.debug("Using csv as output")
            writer = csv.DictWriter(data_file, delimiter = '|', fieldnames=list(data[0].keys()))
            if not file_exists:
                writer.writeheader()
            for row in data:
                writer.writerow(row)
        else:
            log.debug("Using json as output")
            full_data = {"data": data, "meta_data": meta_data}
            data_file.write(json.dumps(full_data))
    return 1

def load(meta_data: dict, path: str, static_name = None, csv_file = False):
    
    if static_name is not None:
        log.info("Loading file with static name {}".format(static_name))
        name = static_name
        path = path + name
    else:
        log.info("Loading file {}".format(str(meta_data)))
        name = create_name(meta_data)
        path = path + name

    if os.path.isfile(path):
        log.info("Loading local data {}".format(name))
        with open(path, newline='') as data_file:
        #data_file = open(path, "r")
            if csv_file:
                data = csv.DictReader(data_file, delimiter = '|', quotechar='|')
                data = list(data)
            else:
                data = json.loads(data_file.read())
                data = data["data"]
        return data
    else:
        log.info("File not found {}".format(path))
        return False
