# Detecting Backup Routes with BGP

Code basis of the bachelor thesis of Maurice Müller

## Info

Querries BGP data and uses an algorithm for detecting backup routes.

The extension contains the code basis for measuring the time delay between two vantage points.

## Installation

* install bgpreader - https://bgpstream.caida.org/docs/install
* create python environment
* install requirements.txt

## Usage

Use the makefile for starting multiple querries.

Inputs:
* v = verbose mode
* p = Vantage point / Peer (i.e. 3320)
* i = timer interval ("2022-04-01 2022-04-03")
* c = collector (rrc01)
* s = using a mrt / kafka file for input
* t = using test data as input
* b = set bundle size (1.0)
* n = create route map with all paths
* as = targeted AS (3320)
* m = route map of the querried data

## Examples

`python3 main.py -v -i "2020-07-17 00:00:00" "2020-07-18 00:00:00" -c rrc11 -b 0.1 -p 9002 -as 13335`
