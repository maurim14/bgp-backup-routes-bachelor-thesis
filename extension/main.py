import argparse
import logging as log
import sys

from multiprocessing import Pool
from src.stream_handler import StreamHandler
from src.process_updates import process_updates
from src.update_evaluation import update_evaluation
from src.zero_delta_t import pre_graph_evaluation as pge
from src.diff_delta_t import diff_heat_map, diff_distribution
from src import storage

DATA_PATH = "data/"

def main(
        time1, 
        time2, 
        collector1, 
        secure, 
        peer1 = None, 
        peer2 = None, 
        collector2 = None, 
        delta_time = None,
        ):
 
    print("\n  ################################")
    print("  #                              #")
    print("  #   BGP-UPD-arrive-time-eval   #")
    print("  #                              #")
    print("  ################################\n")

    if collector2 == None:
        collector2 = collector1

    meta_data = {}
    meta_data["start"] = time1
    meta_data["end"] = time2
    meta_data["collector1"] = collector1
    meta_data["collector2"] = collector2
    meta_data["peer1"] = peer1
    meta_data["peer2"] = peer2

    if not delta_time == "0":
        meta_data["max time difference"] = delta_time

    if secure:
        meta_data["secure_mode"] = secure

    same_upd = storage.load(meta_data, DATA_PATH)
    if not same_upd:
        pool = Pool()
        res1 = pool.apply_async(gather_data, [peer1, time1, time2, collector1])
        res2 = pool.apply_async(gather_data, [peer2, time1, time2, collector2])
        stream1 = res1.get()
        stream2 = res2.get()
        
        #stream2 = StreamHandler(peer2, time1, time2, collector2)
        #if not stream2.load_data():
        #    stream2.collect_data()
        #    stream2.data = process_updates(stream2.data)
        #    stream2.store_data()
        
        same_upd = update_evaluation(stream1.data, stream2.data, safe_mode = secure, delta_time = int(delta_time))
        if len(same_upd) == 0:
            log.error("No Matching Annoucements found")
            exit()

    if peer1 is None or peer2 is None:
        meta_data["peer1"] = stream1.peer
        meta_data["peer2"] = stream2.peer

    # creating a single file for multiple VPs overview
    full_data = storage.load({}, DATA_PATH, static_name = "full_data")
    full_data += same_upd
    storage.store(full_data, {}, DATA_PATH, static_name = "full_data")

    storage.store(same_upd, meta_data, DATA_PATH)

    if delta_time == "0":
        pge(same_upd, meta_data)
    else:
        diff_heat_map(same_upd, meta_data)
        diff_distribution(same_upd, meta_data)

   

def gather_data(peer, time1, time2, collector):
        stream = StreamHandler(peer, time1, time2, collector)
        if not stream.load_data():
            stream.collect_data()
            stream.data = process_updates(stream.data)
            stream.store_data()
        return stream

def plot_full_data(delta_time):

    # creating a single file for multiple VPs overview
    same_upd = storage.load({}, DATA_PATH, static_name = "full_data")
    meta_data = {}

    if same_upd == False:
        log.info("No Data found")
        exit()

    if delta_time == "0":
        pge(same_upd, meta_data)
    else:
        diff_heat_map(same_upd, meta_data)
        diff_distribution(same_upd, meta_data)




if __name__=="__main__":
    parser = argparse.ArgumentParser(
                        prog = 'UPD-time evaluation',
                        description = 'Evaluation of BGP update timestemps')
    parser.add_argument('-v', '--verbose',
                        action='store_true')  # on/off flag
    parser.add_argument('-p', '--peers', nargs=2,
                        default=[None, None])
    parser.add_argument('-c', '--collector', nargs='+',
                        default="rrc01")
    parser.add_argument('-i','--time_interval', nargs=2,
                        default=["2022-04-01 00:00:00", "2022-04-01 00:10:00"])
    parser.add_argument('-s', '--secure',
                        action='store_true')
    parser.add_argument('-t', '--delta_time',
                        default = "0")
    parser.add_argument('-X', '--full_data',
                        action='store_true')

    args = parser.parse_args()
    if args.verbose:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
        log.info("Verbose output.")
    else:
        log.basicConfig(format="%(levelname)s: %(message)s")

    if args.full_data:
        plot_full_data(args.delta_time)
        exit()

    time1 = args.time_interval[0]
    time2 = args.time_interval[1]
    if type(args.collector) == list:
        collector1 = args.collector[0]
        if len(args.collector) == 2:
            collector2 = args.collector[1]
        else:
            collector2 = args.collector[0]
    else:
        collector1 = args.collector
        collector2 = args.collector
    peer1 = args.peers[0]
    peer2 = args.peers[1]
    log.info("Parameters: {} {} {} {} {} {} {} {}".format(time1, time2, collector1, collector2, str(peer1), str(peer2), args.secure, args.delta_time))

    log.getLogger('matplotlib.font_manager').disabled = True

    main(time1 = time1, time2 = time2, collector1 = collector1, collector2 = collector2, peer1 = peer1, peer2 = peer2, secure = args.secure, delta_time = args.delta_time)
