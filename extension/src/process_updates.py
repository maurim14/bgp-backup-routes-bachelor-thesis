import logging as log
    
def process_updates(stream):
    
    count = 0

    grouped_orig = {}
    for elem in stream:
        if not elem.type.lower() == "a":
            continue
        count += 1
        orig = elem._maybe_field("as-path").split(" ")[-1]
        new_elem = {"time": elem.time, "prefix": elem._maybe_field("prefix"), "path": clean_as_path(elem._maybe_field("as-path"))}
        if orig in grouped_orig:
            grouped_orig[orig].append(new_elem)
        else:
            grouped_orig[orig] = [new_elem]

    if count < 50:
        log.warning("very few updates: {}".format(count))
    return grouped_orig


def clean_as_path(path):
    splitted = path.split(" ")
    path_list = []
    for asn in splitted:
        if asn not in path_list:
            path_list.append(asn)
    return " ".join(path_list)
