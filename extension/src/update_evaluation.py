import logging as log
import inspect
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import copy

from multiprocessing import Pool
from matplotlib import mlab
from src.storage import create_name
from itertools import islice

PATH_IMAGES = "images/"
DELTA_TIME = None

def same_data(x, y):
    a = abs(x["time"] - y["time"])
    b = x["prefix"] == y["prefix"]
    c = x["path"].split(" ")[-1] == y["path"].split(" ")[-1]
    if b and c:
        return a

def update_evaluation(data1, data2, safe_mode = False, delta_time = None):
    
    global DELTA_TIME
    DELTA_TIME = delta_time
    pool = Pool()
    
    # divide data1 into 2 parts
    if len(data1.keys()) > 1:
        data_chunks = chunks(data1, len(data1.keys()) //2 + len(data1.keys()) % 2)
        
        log.info("splitting data and start multiple processes")

        res1 = pool.apply_async(multiprocessing_update_evaluation, [next(data_chunks), data2, safe_mode])
        res2 = pool.apply_async(multiprocessing_update_evaluation, [next(data_chunks), data2, safe_mode])

        res1.wait()
        res2.wait()
        return res1.get() + res2.get()
    else:
        return multiprocessing_update_evaluation(data1, data2, safe_mode)

def multiprocessing_update_evaluation(data1, data2, safe_mode = False):
    global DELTA_TIME
    same_update = []
    counter = 0
    for orig in data1:
        if orig in data2:
            for elem2 in data2[orig]:
                
                if safe_mode:
                    minimum = -1
                    min_pos = -1

                for elem1 in data1[orig]:
                    if safe_mode:
                        a = data1[orig]
                        b = data2[orig]

                        secure_filter_same(a)
                        secure_filter_same(b)

                    time_diff = same_data(elem1, elem2)
                    if time_diff == None:
                        continue

                    if safe_mode:
                        if minimum == -1:
                            minimum = time_diff
                            min_pos = elem1
                        elif minimum > time_diff:
                            minimum = time_diff
                            min_pos = elem1
                            
                    elif time_diff < DELTA_TIME:
                        same_update.append((elem1, elem2))
                        counter += 1
                        continue
                
                if safe_mode:
                    if min_pos == -1 or minimum >= DELTA_TIME:
                        continue
                    same_update.append((min_pos, elem2))
                    counter += 1
    
    log.info("Amount mapped updates: {}".format(counter))
    return same_update

def secure_filter_same(data):
    tmp = {}
    for elem in data:
        tup = (elem["time"], elem["prefix"])
        if tup in tmp:
            tmp[tup].append(elem)
        else:
            tmp[tup] = [elem]
    for keys in tmp:
        if len(tmp[keys]) > 1:
            for item in tmp[keys]:
                data.remove(item)

def chunks(data, SIZE=10000):
    it = iter(data)
    for i in range(0, len(data), SIZE):
        yield {k:data[k] for k in islice(it, SIZE)}
