import logging as log
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

from matplotlib import mlab
from src.storage import create_name
from src.general_graphs import text

PATH_IMAGES = "images/"

def pre_graph_evaluation(data, meta_data):

    log.info("mapped updates: {}".format(len(data)))

    branch_orig = [0]
    vp_branch_min = [0]
    vp_branch_max = [0]
    vp_branch_same = [0]
    vp_branch_diff = [0]

    origs = []

    for x,y in data:
        a = x["path"].split(" ")
        b = y["path"].split(" ")
        a.reverse()
        b.reverse()

        if a[-1] not in origs:
            origs.append(a[-1])
        if b[-1] not in origs:
            origs.append(b[-1])

        branch = -1
        for i in range(0,len(a) -1):
            if i == len(b):
                break
            if a[i] == b[i]:
                branch = i
        else:
            if len(a) == 1:
                branch = 0

        if branch == -1:
            log.error("Branch point wrong", a, b)

        val2 = len(a) - branch - 1
        val3 = len(b) - branch - 1
        val4 = abs(val3 - val2)

        branch_orig.append(branch)
        vp_branch_diff.append(val4)
        if val4 > 2:
            logger(x,y)

        if val4 == 0:
            vp_branch_same.append(val2)

        else:
            if val3 > val2:
                vp_branch_max.append(val3)
                vp_branch_min.append(val2)
            else:
                vp_branch_max.append(val2)
                vp_branch_min.append(val3)

    log.info("Amount vp_branch_min: {}".format(len(vp_branch_max)))

    if len(origs) > 2:
        log.error("amount VPs > 2")

    xlabel = "AS hops"
    ylabel = "CDF"
    curve1_label = "dist(branch, origin)"
    curve2_label = "diff(dist(vp1, branch), dist(vp2, branch))"
    curve3_label = "max(dist(vp1, branch), dist(vp2, branch))"
    curve4_label = "min(dist(vp1, branch), dist(vp2, branch))"
    curve5_label = "dist(vp1, branch) == dist(vp2, branch)"

    title = "Comparison: arrive time of update at different VPs"
    textparams = text(meta_data)

    function_name = inspect.stack()[0][3] + "/"
    time = "" #side_data["start"] + side_data["end"]
    store_image_path = PATH_IMAGES + function_name + create_name(meta_data) + ".png"

    time_abs = max(max(vp_branch_max),max(vp_branch_same), max(branch_orig), max(vp_branch_diff))
    # adding a the max element to all data for synchronisation
    vp_branch_max.append(time_abs)
    vp_branch_min.append(time_abs)
    vp_branch_same.append(time_abs)
    branch_orig.append(time_abs)
    vp_branch_diff.append(time_abs)

    time_bin = 1
    n_bins = int(time_abs / time_bin)

    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)

    plt.subplots_adjust(left=0.3, bottom = 0.15)

    # plot the cumulative histogram primary_routes
    n, bins, patches = plt.hist(branch_orig, n_bins, histtype='step', linewidth=2,
                               cumulative=True, label=curve1_label,
                               weights=np.ones_like(branch_orig) / len(branch_orig))
                               #weights=np.ones_like(vp_branch_max) / len(branch_orig))
    n, bins, patches = plt.hist(vp_branch_diff, n_bins, histtype='step', linewidth=2,
                               cumulative=True, label=curve2_label,
                               weights=np.ones_like(vp_branch_diff) / len(vp_branch_diff))
                               #weights=np.ones_like(vp_branch_max) / len(vp_branch_diff))
    if len(vp_branch_min) > 2:
        n, bins, patches = plt.hist(vp_branch_max, n_bins, histtype='step', linewidth=2,
                               cumulative=True, label=curve3_label,
                               weights=np.ones_like(vp_branch_min) / len(vp_branch_min))
                               #weights=np.ones_like(vp_branch_max) / len(vp_branch_max))
        n, bins, patches = plt.hist(vp_branch_min, n_bins, histtype='step', linewidth=2,
                               cumulative=True, label=curve4_label,
                               weights=np.ones_like(vp_branch_min) / len(vp_branch_min))
                               #weights=np.ones_like(vp_branch_max) / len(vp_branch_min))
    n, bins, patches = plt.hist(vp_branch_same, n_bins, histtype='step', linewidth=2,
                               cumulative=True, label=curve5_label,
                               weights=np.ones_like(vp_branch_same) / len(vp_branch_same))
                               #weights=np.ones_like(vp_branch_max) / len(vp_branch_same))

    #y = mlab.normpdf(bins, 200, 25).cumsum()
    #y /= y[-1]

    #ax.plot(bins, y, 'k--', linewidth=1.5, label='Theoretical')

    # Overlay a reversed cumulative histogram.
    #ax.hist(x, bins=bins, histtype='step', cumulative=-1,

    plt.gcf().text(0.02, 0.5, textparams, fontsize=14)
    plt.grid(True)
    plt.legend(loc='right')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(store_image_path)
    plt.show()

    log.info("Image path: {}".format(store_image_path))


def logger(x,y):
    a = open("logger_great_diff","a")
    texte = str(x["time"]) + "|" + str(x["prefix"]) + "|" + str(x["path"]) + "|" + str(y["path"]) + "\n"
    a.write(texte)

