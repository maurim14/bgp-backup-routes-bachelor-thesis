import logging as log
import hashlib
import json
import os

def create_name(meta_data: dict):
    log.info("Hashing {}".format(str(meta_data)))
    m = hashlib.sha256()
    m.update(json.dumps(meta_data).encode("ascii"))
    name = str(m.hexdigest())
        
    log.info("Hash Output {}".format(name))
    return name

def store(data: dict, meta_data: dict, path: str, static_name = None):

    if static_name is not None:
        log.info("Storing file content with static name {}".format(static_name))
        name = static_name
        path = path + name
    else:
        log.info("Storing file content {}".format(str(meta_data)))
        name = create_name(meta_data)
        path = path + name

    if os.path.isfile(path):
            log.info("Data already locally stored")
            if static_name is not None:
                log.warning("Replacing existing static_name file")
                os.remove(path)
            else:
                return 0

    log.info("Storing data {}".format(name))
    data_file = open(path, "x")

    full_data = {"data": data, "meta_data": meta_data}

    return data_file.write(json.dumps(full_data))


def load(meta_data: dict, path: str, static_name = None):
    
    if static_name is not None:
        log.info("Loading file with static name {}".format(static_name))
        name = static_name
        path = path + name
    else:
        log.info("Loading file {}".format(str(meta_data)))
        name = create_name(meta_data)
        path = path + name

    if os.path.isfile(path):
        log.info("Loading local data {}".format(name))
        data_file = open(path, "r")
        data = json.loads(data_file.read())
        data = data["data"]
        return data
    else:
        log.info("File not found {}".format(path))
        return False
