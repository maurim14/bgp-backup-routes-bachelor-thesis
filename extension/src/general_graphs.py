def text(params: dict):
    text = ""

    for key in params.keys():
        text += str(key) + ": " + str(params[key]) + "\n"

    return text[:-1]
