import pybgpstream
import random
import datetime
import logging as log

from src import storage

from dateutil.parser import parse

DATA_PATH = "data/"

class StreamHandler:

    def __init__(self, peer: int = None, time_start = "2022-04-01 00:00:00", time_end = "2022-04-01 00:01:00", collector = "rrc01"):
        self.time_start = time_start
        self.time_end = time_end
        self.collector = collector
        self.peer = str(peer) if peer is not None else self.get_random_peer()

        log.info("time_start {}".format(self.time_start))
        log.info("time_end {}".format(self.time_end))
        log.info("collector {}".format(self.collector))
        log.info("peer {}".format(self.peer))

    def get_random_peer(self):

        log.debug("choosing random peer")
        until_time = str(datetime.datetime.fromtimestamp(parse(self.time_start).timestamp() + 60))
        stream = pybgpstream.BGPStream(
                                       from_time = self.time_start,
                                       collector = self.collector,
                                       until_time = until_time,
                                       record_type = "updates",
                                       )
        asn = []
        for elem in stream:
            if elem.peer_asn not in asn:
                asn.append(elem.peer_asn)

        return asn[random.randint(0,len(asn) - 1)]


    def collect_data(self):

        log.info("loading remote data")
        stream = pybgpstream.BGPStream(
                                         from_time = self.time_start,
                                         until_time = self.time_end,
                                         collector = self.collector,
                                         record_type = "updates",
                                         filter = "peer " + str(self.peer),
                                         )
        self.data = stream
        return True

    def meta_data(self):
        meta = {}
        meta["time_start"] = self.time_start
        meta["time_end"] = self.time_end
        meta["collector"] = self.collector
        meta["peer"] = self.peer
        return meta

    def store_data(self):
        
        meta_data = self.meta_data()
        return storage.store(self.data, meta_data, DATA_PATH)

    def load_data(self):

        meta_data = self.meta_data()
        self.data = storage.load(meta_data, DATA_PATH)
        return self.data
