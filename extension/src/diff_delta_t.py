import logging as log
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import inspect
import math
import matplotlib
import scipy.stats as stats
import pylab as pl
import seaborn as sns

from matplotlib import mlab
from src.storage import create_name
from src.general_graphs import text
from statistics import mean

HALF_MATRIX = False
MAX_HOPS = 9
PATH_IMAGES = "images/"

DIFF_HOPS_THRESHOLD = 2
CLOSE_VP_THRESHOLD = 2
FAR_VP_THRESHOLD = 4

def diff_heat_map(data, meta_data):
    
    log.info("Creating heat map with MAX_HOPS = {}".format(MAX_HOPS))

    origs = []
    matrix = [[[] for x in range(MAX_HOPS)] for x in range(MAX_HOPS)]
    
    data_matrix = [[[] for x in range(MAX_HOPS)] for x in range(MAX_HOPS)]

    log.info("Data preprocessing started")
    for x,y in data:
        a = x["path"].split(" ")
        b = y["path"].split(" ")
        a.reverse()
        b.reverse()

        if a[-1] not in origs:
            origs.append(a[-1])
        if b[-1] not in origs:
            origs.append(b[-1])

        branch = -1
        for i in range(0,len(a) -1):
            if i == len(b):
                break
            if a[i] == b[i]:
                branch = i
        else:
            if len(a) == 1:
                branch = 0

        if branch == -1:
            log.error("Branch point wrong", a, b)

        vp1_branch = len(a) - branch - 1
        vp2_branch = len(b) - branch - 1
        
        time = abs(x["time"] - y["time"])
        if vp1_branch >= MAX_HOPS or vp2_branch >= MAX_HOPS:
            continue
        
        if HALF_MATRIX and vp2_branch < vp1_branch:
                matrix[vp2_branch][vp1_branch].append(time)
        else:
            matrix[vp1_branch][vp2_branch].append(time)

    for x_column in range(len(matrix)):
        x = matrix[x_column]
        
        for y_column in range(len(x)):
            if len(matrix[x_column][y_column]) == 0:
                mean_val = np.nan
                data_matrix[x_column][y_column] = np.nan
            else:
                mean_val = mean(matrix[x_column][y_column])
                data_matrix[x_column][y_column] = len(matrix[x_column][y_column])
            
            matrix[x_column][y_column] = mean_val

    if len(origs) > 2:
        log.error("amount VPs > 2")

    # defining graph values
    x_values = [x for x in range(MAX_HOPS -1 , -1, -1)]
    y_values = [x for x in range(0, MAX_HOPS)]
    matrix = np.array(matrix)
    
    xlabel = "Distance VP2 to branch [#hops]" 
    ylabel = "Distance VP1 to branch [#hops]"

    log.info("Creating graph")
    # graph generation
    textparams = text(meta_data)

    function_name = inspect.stack()[0][3] + "/"
    store_image_path = PATH_IMAGES + function_name + create_name(meta_data) + ".png"


    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)
    
    ax = sns.heatmap(matrix, annot=True, cmap = "Greens", cbar_kws={'label': 'Δt [s]'})
    ax.invert_yaxis()
    plt.subplots_adjust(left=0.3, bottom = 0.15)
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0, horizontalalignment='right')
    #fig, ax = plt.subplots()
    #im, cbar = heatmap(matrix, x_values, y_values, ax=ax,
    #                   cmap="YlGn", cbarlabel="time difference [seconds]", vmin=0)
    #texts = annotate_heatmap(im, valfmt="{x:.1f} s")

    plt.gcf().text(0.02, 0.5, textparams, fontsize=14)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(store_image_path)
    plt.show()

    data_matrix = np.array(data_matrix)

    ax = sns.heatmap(data_matrix, annot=True, cmap="Greens", cbar_kws={'label': 'announcements [amount]'}, fmt='g')
    ax.invert_yaxis()
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0, horizontalalignment='right')
    #fig, ax = plt.subplots()
    #im, cbar = heatmap(data_matrix, x_values, y_values, ax=ax,
    #                   cmap="YlGn", cbarlabel="data set amount")
    #texts = annotate_heatmap(im, valfmt="{x:.1f}")

    plt.show()

def diff_distribution(data, meta_data):
        
    log.info("Creating distribution with MAX_HOPS = {}".format(MAX_HOPS))

    origs = []

    case1 = []
    case2 = []
    case3 = []

    log.info("Data preprocessing started")
    for x,y in data:
        a = x["path"].split(" ")
        b = y["path"].split(" ")
        a.reverse()
        b.reverse()

        if a[-1] not in origs:
            origs.append(a[-1])
        if b[-1] not in origs:
            origs.append(b[-1])

        branch = -1
        for i in range(0,len(a) -1):
            if i == len(b):
                break
            if a[i] == b[i]:
                branch = i
        else:
            if len(a) == 1:
                branch = 0

        if branch == -1:
            log.error("Branch point wrong", a, b)

        vp1_branch = len(a) - branch - 1
        vp2_branch = len(b) - branch - 1
        diff_hops = abs(vp1_branch - vp2_branch)

        time = abs(x["time"] - y["time"])
        #if DIFF_HOPS_THRESHOLD <= diff_hops:
        #    case2.append(time)
        #elif vp1_branch <= CLOSE_VP_THRESHOLD and vp2_branch <= CLOSE_VP_THRESHOLD:
        #    case1.append(time)
        #else:
        #    case3.append(time)a

        if vp1_branch <= CLOSE_VP_THRESHOLD and vp2_branch <= CLOSE_VP_THRESHOLD:
            case1.append(time)
        elif vp1_branch >= FAR_VP_THRESHOLD and vp2_branch >= FAR_VP_THRESHOLD:
            case3.append(time)
        else:
            case2.append(time)

    if len(origs) > 2:
        log.error("amount VPs > 2")

    # defining graph values
    xlabel = "time difference announcement arriving [seconds]" 
    ylabel = "relative distribution [normed to 1]"

    log.info("Creating graph")
    # graph generation
    textparams = text(meta_data)

    function_name = inspect.stack()[0][3] + "/"
    store_image_path = PATH_IMAGES + function_name + create_name(meta_data) + ".png"

    mpl.rcParams['font.size'] = 14
    mpl.rcParams['figure.dpi'] = 100
    mpl.rcParams["figure.figsize"] = (15,5)
    
    plt.subplots_adjust(left=0.3, bottom = 0.15)

    case1 = sorted(case1)
    case2 = sorted(case2)
    case3 = sorted(case3)
 
    fit1 = stats.norm.pdf(case1, np.mean(case1), np.std(case1))  #this is a fitting indeed
    fit2 = stats.norm.pdf(case2, np.mean(case2), np.std(case2))  #this is a fitting indeed
    fit3 = stats.norm.pdf(case3, np.mean(case3), np.std(case3))  #this is a fitting indeed
    
    pl.plot(case1,fit1,'-o', label="case 1")
    pl.plot(case2,fit2,'-o', label="case 2")
    pl.plot(case3,fit3,'-o', label="case 3")

    plt.gcf().text(0.02, 0.5, textparams, fontsize=14)
    plt.legend(loc='right')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(store_image_path)
    plt.show()

def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw=None, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (M, N).
    row_labels
        A list or array of length M with the labels for the rows.
    col_labels
        A list or array of length N with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if ax is None:
        ax = plt.gca()

    if cbar_kw is None:
        cbar_kw = {}

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # Show all ticks and label them with the respective list entries.
    ax.set_xticks(np.arange(data.shape[1]), labels=col_labels)
    ax.set_yticks(np.arange(data.shape[0]), labels=row_labels)

    # Let the horizontal axes labeling appear on top.
    #ax.tick_params(top=False, bottom=True,
    #              labeltop=False, labelbottom=True)

    # Rotate the tick labels and set their alignment.
    # plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
    #         rotation_mode="anchor")

    # Turn spines off and create white grid.
    ax.spines[:].set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=("black", "white"),
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            # changing the value -1 to escape value
            #if data[i,j] == -1:
            #    continue

            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts
