import argparse
import logging as log

from src.stream_handler import StreamHandler
from src.rib_reader import read_rib
from src.create_test_data import create_test
from src.evaluate_announcements import evaluate_announcements
from src.traffic_map import traffic_map, add_to_traffic_map
from src.helper_functions import strong_dataset
from src import storage


PATH_RIB_DATA = "data/ribs/"
PATH_PROCESSED_DATA = "data/switches/"

def all_paths(
    time_start: str, 
    time_end: str, 
    asn = None, 
    test = None, 
    one_hour = False, 
    collector = None,
    target_as = None,
    route_map = None,
        ):
    """
    Get BGP Updates and get all available paths and display them in a route_map if wanted
    """

    print("\n###########################")
    print("#         ALL PATHS       #")
    print("###########################\n")

    if test is not None:
        test = create_test()

    meta_data = {}
    meta_data["start"] = time_start
    meta_data["end"] = time_end
    meta_data["collector"] = collector
    meta_data["peer"] = peer
    meta_data["target AS"] = target_as
    if test is not None:
        meta_data["test case"] = str(True)

"""

    detections = no_switch_algorithm(
                                 time_start + RIB_TIME_START1,
                                 time_start + RIB_TIME_END1,
                                 time_start + ANNOUNCEMENT_START,
                                 time_end + ANNOUNCEMENT_START,
                                 asn = asn,
                                 test_data = test,
                                 collector = collector,
                                 )
    vp = str(detections["vp"])
    log.info("Storing all Findings")

    if test is not None:
        handler.store_data(detections, PATH_FINDING + "all-paths" + "TEST" + time_start + time_end + collector + vp)
    else:
        handler.store_data(detections, PATH_FINDING + "all-paths" + time_start + time_end + collector + vp)

    log.info("Done")
"""

def find_switches(
        time_start, 
        time_end, 
        peer = None, 
        test = None, 
        singlefile = False,
        collector = None,
        bundle_size = None,
        target_as = None,
        route_map = None,
        ):
    """
    Collect BGP Announcements to find Backup routes via an algorithm
    """
    print("\n###########################")
    print("#      FIND SWITCHES      #")
    print("###########################\n")

    log.info("Start: " + time_start)
    log.info("End: "+ time_end)

    log.debug("Collector: {}".format(collector))
    log.debug("Prefix Bundle Size: {}".format(bundle_size))
    log.debug("Singlefile: {}".format(singlefile))
    log.debug("Test: {}".format(test))
    log.debug("target AS: {}".format(target_as))
    log.debug("route map: {}".format(route_map))
    log.debug("VP peer: {}".format(peer))

    meta_data = {}
    meta_data["start"] = time_start 
    meta_data["end"] = time_end
    meta_data["peer"] = peer
    meta_data["collector"] = collector 
    meta_data["singlefile"] = str(singlefile)
    meta_data["target_as"] = target_as 
    meta_data["bundle size"] = bundle_size

    bundle_size = float(bundle_size)
    newly_processed = False

    if test:
        meta_data["test"] = True
        test = create_test()

    # check if switches have been evaluated
    local_data_switches = storage.load(meta_data, PATH_PROCESSED_DATA)
    if not local_data_switches:
        
        #check if RIB is present
        newly_processed = True
        rib_stream = StreamHandler(target_as, time_start, time_end, collector, peer)
        rib_data = rib_stream.load_data()
        meta_data["peer"] = str(rib_stream.peer)

        if not rib_data:

            #load RIB
            rib_stream.collect_data_rib()
            rib_data = read_rib(rib_stream.rib_data)

            # Storing RIB Data
            rib_stream.rib_data = rib_data
            rib_stream.store_data()

        # load announcements and find switches
        rib_stream.collect_data_updates()
        upd_data = rib_stream.data
        local_data_switches = evaluate_announcements(upd_data, rib_stream.rib_data, bundle_size)

        # Store found switches
        storage.store(local_data_switches, meta_data, PATH_PROCESSED_DATA)

    # all needed data is there
    log.info("Data querried and ready for postprocessing")

    # Store data to roadmap
    if target_as is not None:
        add_to_traffic_map(local_data_switches, meta_data)

    # full-switch statistics
    if newly_processed and target_as is not None:
        all_switches = storage.load({},PATH_PROCESSED_DATA, static_name = "all_switches" + target_as)
        if all_switches == False:
            log.error("File missing / Naming incorrect")
            all_switches = {"findings": [], "half-findings": []}
        else:
            all_switches["findings"].append(local_data_switches["finding"])
            all_switches["half-findings"].append(local_data_switches["half-finding"])
        storage.store(all_switches, {}, PATH_PROCESSED_DATA, static_name= "all_switches" + target_as)

    # full-switch combined strong-dataset
    local_data_switches = strong_dataset(local_data_switches, meta_data)
    if newly_processed:
        if len(local_data_switches["findings"]) > 0:
            storage.store(local_data_switches["findings"], {}, PATH_PROCESSED_DATA, static_name= "strong_dataset", csv_file = True)
        if len(local_data_switches["half-findings"]) > 0:
            storage.store(local_data_switches["half-findings"], {}, PATH_PROCESSED_DATA, static_name= "strong_dataset", csv_file = True)

    # draw data as route-map
    if route_map:
        traffic_map(meta_data, route_map)

    print("Backup Cases: {}".format(len(local_data_switches["findings"])))
    print("Half Backup Cases: {}".format(len(local_data_switches["half-findings"])))

def parse_arguments():
    parser = argparse.ArgumentParser(
                        prog = 'BGP Switch Search',
                        description = 'Find bundles of prefixes switching paths simmultaneously')
    parser.add_argument('-v', '--verbose',
                        action='store_true')  # on/off flag
    parser.add_argument('-p', '--peer', 
                        default = None
                        )
    parser.add_argument('-i', '--time_interval', nargs=2,
                        default=["2022-04-01", "2022-04-02"])
    parser.add_argument('-c', '--collector',
                        default="rrc01")
    parser.add_argument('-s', '--singlefile',
                        action='store_true')
    parser.add_argument('-t', '--test',
                        action='store_true')
    parser.add_argument('-b', '--bundle_size',
                        default=1.0)
    parser.add_argument('-n', '--all_paths',
                        action='store_true', default=False)
    parser.add_argument('-as', '--target_as',
                        default="3320")
    parser.add_argument('-m', '--route_map',
                        default = False,
                        )

    args = parser.parse_args()
    if args.verbose:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
        log.info("Verbose output.")
    else:
        log.basicConfig(format="%(levelname)s: %(message)s")

    # no-switch flag set True
    if args.all_paths:
        all_paths(
                    time_start = args.time_interval[0],
                    time_end = args.time_interval[1], 
                    peer = args.peer,
                    test = args.test,
                    collector = args.collector,
                    target_as = args.target_as,
                    route_map = args.route_map
                    )
    else:
        find_switches(
                    time_start = args.time_interval[0],
                    time_end = args.time_interval[1],
                    peer = args.peer,
                    test = args.test,
                    singlefile = args.singlefile,
                    collector = args.collector,
                    bundle_size = args.bundle_size,
                    target_as = args.target_as,
                    route_map = args.route_map,
                    )

if __name__=="__main__":
    parse_arguments()
